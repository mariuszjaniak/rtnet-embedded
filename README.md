# RTnet Embedded #

The Real Time networking for embedded devices. Main features 

* fully compatible with original RTnet (http://www.rtnet.org), that recently has been integrated with Xenomai 3.x (http://xenomai.org)
* socket style API,
* implements: RTmac, RTcfg, TDMA, UDP/IP and ARP,
* written in plain C, 
* integrated with FreeRTOS, 
* supported devices: TI Stellaris lm3s8xxx, Infineon XMC45xx,
* supported compilers: gcc, TI CCS,
* portable is easy, through simple HAL.