/*****************************************************************************
 *                                                                           *
 *   rtcfg.c                                                                 *
 *                                                                           *
 *   RTcfg -- real-time configuration service for RTnet                      *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

/* RTnet includes */
#include "rtnet_struct.h"
#include "rtnet_inet.h"
#include "rtnet_mem.h"
#include "rtnet_private.h"
#include "halRTcfg.h"
#include "rtcfg.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtcfgFRAME_ID_MASK               0x1F

#define rtcfgFRAME_ID_CONFIGURATION      0
#define rtcfgFRAME_ID_NEW_ANNOUNCEMENT   1
#define rtcfgFRAME_ID_REPLY_ANNOUNCEMENT 2
#define rtcfgFRAME_ID_INITIAL            3
#define rtcfgFRAME_ID_SUBSEQUENT         4
#define rtcfgFRAME_ID_ACKNOWLEDGE        5
#define rtcfgFRAME_ID_READY              6
#define rtcfgFRAME_ID_HEARTBEAT          7
#define rtcfgFRAME_ID_DEAD_STATION       8

#define rtcfgADDR_MAC                    0
#define rtcfgADDR_IP                     1

#define rtcfgFLAG_STAGE2_DATA            0x01
#define rtcfgFLAG_READY                  0x02

#define rtcfgADDRSIZE_IP                 4

#define rtcfgMAX_DELAY                   10

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

RTNET_STRUCT_BEGIN();
struct xRTCFG_HEADER{
  uint8_t ucId;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_HEADER xRTcfgHeader_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_CONFIGURATION {
  xRTcfgHeader_t  xHead;
  uint8_t         ucAddrType;
  uint8_t         pucClientAddr[0];
  uint8_t         pucServerAddr[0];
  uint8_t         ucBurstRate;
  uint16_t        usCfgLen;
  uint8_t         pucCfgData[0];
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_CONFIGURATION xRTcfgConfiguration_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_NEW_ANNOUNCEMENT {
  xRTcfgHeader_t  xHead;
  uint8_t         ucAddrType;
  uint8_t         pucClientAddr[0];
  uint8_t         ucFlags;
  uint8_t         ucBurstRate;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_NEW_ANNOUNCEMENT xRTcfgNewAnnouncement_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_REPLY_ANNOUNCEMENT {
  xRTcfgHeader_t  xHead;
  uint8_t         ucAddrType;
  uint8_t         pucClientAddr[0];
  uint8_t         ucFlags;
  uint8_t         ucPadding;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_REPLY_ANNOUNCEMENT xRTcfgReplyAnnouncement_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_INITIAL {
  xRTcfgHeader_t  xHead;
  uint8_t         ucFlags;
  uint32_t        ulActiveStation;
  uint16_t        usHeartbeat;
  uint32_t        ulCfgLen;
  uint8_t         pucCfgData[0];
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_INITIAL xRTcfgInitial_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_SUBSEQUENT {
  xRTcfgHeader_t  xHead;
  uint32_t        ulOffset;
  uint8_t         pucCfgData[0];
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_SUBSEQUENT xRTcfgSubsequent_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_ACKNOWLEDGE {
  xRTcfgHeader_t  xHead;
  uint32_t        ulAckLen;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_ACKNOWLEDGE xRTcfgAcknowledge_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_READY {
  xRTcfgHeader_t  xHead;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_READY xRTcfgReady_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_HEARTBEAT {
  xRTcfgHeader_t  xHead;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_HEARTBEAT xRTcfgHeartbeat_t;

RTNET_STRUCT_BEGIN();
struct xRTCFG_DEAD_STATION {
  xRTcfgHeader_t  xHead;
  uint8_t         ucAddrType;
  uint8_t         pucLogClientAddr[0];
  uint8_t         pucPhyClientAddr[32];
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTCFG_DEAD_STATION xRTcfgDeadStation_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static BaseType_t  prvRxConfiguration(xRTcfgDev_t           *pxDev,
                                        xRTcfgConfiguration_t *pxFrame,
                                        uint8_t               *pucSource,
                                        uint8_t               *pucDestination);
static BaseType_t  prvRxNewAnnouncement(xRTcfgDev_t             *pxDev,
                                          xRTcfgNewAnnouncement_t *pxFrame,
                                          uint8_t                 *pucSource);
static BaseType_t  prvRxReplyAnnouncement(xRTcfgDev_t               *pxDev,
                                            xRTcfgReplyAnnouncement_t *pxFrame,
                                            uint8_t                   *pucSource);
static BaseType_t  prvRxInitial(xRTcfgDev_t     *pxDev,
                                  xRTcfgInitial_t *pxFrame,
                                  uint32_t         ulLen,
                                  uint8_t         *pucSource);
static BaseType_t  prvRxSubsequent(xRTcfgDev_t        *pxDev,
                                     xRTcfgSubsequent_t *pxFrame,
                                     uint32_t            ulLen);
static BaseType_t  prvRxAcknowledge(xRTcfgDev_t         *pxDev,
                                      xRTcfgAcknowledge_t *pxFrame,
                                      uint8_t             *pucSource);
static BaseType_t  prvRxReady(xRTcfgDev_t   *pxDev,
                                xRTcfgReady_t *pxFrame,
                                uint8_t       *pucSource);
static BaseType_t  prvRxHeartbeat(xRTcfgDev_t       *pxDev,
                                    xRTcfgHeartbeat_t *pxFrame,
                                    uint8_t           *pucSource);
static BaseType_t  prvRxDeadStation(xRTcfgDev_t         *pxDev,
                                      xRTcfgDeadStation_t *pxFrame);
static BaseType_t  prvTxNewAnnouncement(xRTcfgDev_t *pxDev);
static BaseType_t  prvTxReplyAnnouncement(xRTcfgDev_t *pxDev,
                                            uint8_t     *pucMac);
static BaseType_t  prvTxAcknowledge(xRTcfgDev_t *pxDev);
static BaseType_t  prvTxReady(xRTcfgDev_t *pxDev);
static BaseType_t  prvTxHeartbeat(xRTcfgDev_t *pxDev);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

static const uint8_t gpucBroadcast[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTcfgInit(xRTcfgDev_t *pxDev)
{
  if(pxDev == NULL) return pdFAIL;
  pxDev->ulIpAddr      = 0;
  pxDev->usHeartbeat   = 0;
  pxDev->ucStage2Frame = 0;
  pxDev->ulStage2Data  = 0;
  pxDev->ucBurstRate   = rtnetconfigRTCFG_BURST_RATE;
  pxDev->eAddr         = eRTcfgAddrIP;
  pxDev->eState        = eRTcfgStateInit;
  memset(pxDev->pucMaster, 0, rtcfgMAC_SIZE);
  if(xRTarpInit(&pxDev->xArp) == pdFAIL) return pdFAIL;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgProcessFrame(xRTcfgDev_t *pxDev,
                                 uint8_t      pucFrame[],
                                 uint32_t     ulLen,
                                 uint8_t     *pucSource,
                                 uint8_t     *pucDestination)
{
  BaseType_t  xReturn = pdPASS;
  xRTcfgHeader_t *pxHeader = (xRTcfgHeader_t*) pucFrame;

  switch(pxHeader->ucId & rtcfgFRAME_ID_MASK){
  case rtcfgFRAME_ID_CONFIGURATION:
    xReturn = prvRxConfiguration(pxDev,
                                 (xRTcfgConfiguration_t*) pucFrame,
                                 pucSource,
                                 pucDestination);
    break;
  case rtcfgFRAME_ID_NEW_ANNOUNCEMENT:
    xReturn = prvRxNewAnnouncement(pxDev,
                                   (xRTcfgNewAnnouncement_t*) pucFrame,
                                   pucSource);
    break;
  case rtcfgFRAME_ID_REPLY_ANNOUNCEMENT:
    xReturn = prvRxReplyAnnouncement(pxDev,
                                    (xRTcfgReplyAnnouncement_t*) pucFrame,
                                    pucSource);
    break;
  case rtcfgFRAME_ID_INITIAL:
    xReturn = prvRxInitial(pxDev,
                           (xRTcfgInitial_t*) pucFrame,
                           ulLen,
                           pucSource);
    break;
  case rtcfgFRAME_ID_SUBSEQUENT:
    xReturn = prvRxSubsequent(pxDev,
                              (xRTcfgSubsequent_t*) pucFrame,
                              ulLen);
    break;
  case rtcfgFRAME_ID_ACKNOWLEDGE:
    xReturn = prvRxAcknowledge(pxDev,
                               (xRTcfgAcknowledge_t*) pucFrame,
                               pucSource);
    break;
  case rtcfgFRAME_ID_READY:
    xReturn = prvRxReady(pxDev,
                         (xRTcfgReady_t*) pucFrame,
                         pucSource);
    break;
  case rtcfgFRAME_ID_HEARTBEAT:
    xReturn = prvRxHeartbeat(pxDev,
                             (xRTcfgHeartbeat_t*) pucFrame,
                             pucSource);
    break;
  case rtcfgFRAME_ID_DEAD_STATION:
    xReturn = prvRxDeadStation(pxDev,
                               (xRTcfgDeadStation_t*) pucFrame);
    break;
  default:
    return pdFAIL;
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgSendAcknowledge(xRTcfgDev_t *pxDev)
{
  return prvTxAcknowledge(pxDev);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgSendReady(xRTcfgDev_t *pxDev)
{
  return prvTxReady(pxDev);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgSendHearbeat(xRTcfgDev_t *pxDev)
{
  return prvTxHeartbeat(pxDev);
}
/*---------------------------------------------------------------------------*/

size_t xRTcfgFrameSize(eRTcfgFrame_t eFrameType)
{
  switch(eFrameType){
  case eRTcfgFrameConfiguration:
    return sizeof(xRTcfgConfiguration_t);
  case eRTcfgFrameNewAnnouncement:
    return sizeof(xRTcfgNewAnnouncement_t);
  case eRTcfgFrameReplyAnnouncement:
    return sizeof(xRTcfgReplyAnnouncement_t);
  case eRTcfgFrameInitial:
    return sizeof(xRTcfgInitial_t);
  case eRTcfgFrameSubsequent:
    return sizeof(xRTcfgSubsequent_t);
  case eRTcfgFrameAcknowledge:
    return sizeof(xRTcfgAcknowledge_t);
  case eRTcfgFrameReady:
    return sizeof(xRTcfgReady_t);
  case eRTcfgFrameHeartbeat:
    return sizeof(xRTcfgHeartbeat_t);
  case eRTcfgFrameDeadStation:
    return sizeof(xRTcfgDeadStation_t);
  }
  return 0;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgRouteAdd(xRTcfgDev_t *pxDev,
                             uint8_t      pucMac[],
                             uint32_t     ulIpAddr)
{
  BaseType_t  xRet;

  xRet = xRTarpAdd(&pxDev->xArp, pucMac, ulIpAddr);
  if(xRet == pdFAIL) return pdFAIL;
  return xRTarpSetState(&pxDev->xArp, pucMac, eRTarpStateReady);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgRouteDelMac(xRTcfgDev_t *pxDev, uint8_t pucMac[])
{
  return xRTarpDelMac(&pxDev->xArp, pucMac);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTcfgRouteDelIp(xRTcfgDev_t *pxDev, uint32_t ulIpAddr)
{
  return xRTarpDelIp(&pxDev->xArp, ulIpAddr);
}
/*---------------------------------------------------------------------------*/

uint8_t *pucRTcfgGetMacPtr(xRTcfgDev_t *pxDev, uint32_t ulIpAddr)
{
  return pucRTarpGetMacPtr(&pxDev->xArp, ulIpAddr);
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxConfiguration(xRTcfgDev_t           *pxDev,
                                 xRTcfgConfiguration_t *pxFrame,
                                 uint8_t               *pucSource,
                                 uint8_t               *pucDestination)
{
  BaseType_t  xReturn = pdPASS;

  if(pxDev->eState == eRTcfgStateReady){
    /* Set new master MAC address */
    memcpy(pxDev->pucMaster, pucSource, rtcfgMAC_SIZE);
    return prvTxReplyAnnouncement(pxDev, pucSource);
  }
  switch(pxFrame->ucAddrType){
  case rtcfgADDR_MAC:
    /* Set master MAC address */
    memcpy(pxDev->pucMaster, pucSource, rtcfgMAC_SIZE);
    /* Configure device */
    pxDev->eAddr = eRTcfgAddrMAC;
    /* Add master to ARP table */
    if(xRTarpAdd(&pxDev->xArp, pucSource, rtarpWITHOUT_IP) == pdFAIL)
      return pdFAIL;
    pxDev->ucBurstRate = RTNET_MIN(pxFrame->ucBurstRate, pxDev->ucBurstRate);
    /* Process data */
    xReturn = xHalRTcfgProcessStage1Data(pxFrame->pucCfgData,
                                         rtnet_ntohs(pxFrame->usCfgLen));
    break;
  case rtcfgADDR_IP:
    /* Skip frame if broadcast */
    if(memcmp(gpucBroadcast, pucDestination, rtcfgMAC_SIZE) == 0)
      return pdPASS;
    /* Set master MAC address */
    memcpy(pxDev->pucMaster, pucSource, rtcfgMAC_SIZE);
    /* Configure device */
    pxDev->eAddr = eRTcfgAddrIP;
    {
      uint32_t ulAddr;
      memcpy(&ulAddr, pxFrame->pucClientAddr, sizeof(uint32_t));
      /* Set device IP address */
      //pxDev->ulIpAddr = rtnet_ntohl(ulAddr);
      pxDev->ulIpAddr = ulAddr;
    }
    pxFrame = (xRTcfgConfiguration_t *)
        (((uint8_t *)pxFrame) + rtcfgADDRSIZE_IP);
    {
      uint32_t ulAddr;
      memcpy(&ulAddr, pxFrame->pucServerAddr, sizeof(uint32_t));
      //ulAddr = rtnet_ntohl(ulAddr);
      /* Add master to ARP table */
      if(xRTarpAdd(&pxDev->xArp, pucSource, ulAddr) == pdFAIL) return pdFAIL;
    }
    pxFrame = (xRTcfgConfiguration_t *)
            (((uint8_t *)pxFrame) + rtcfgADDRSIZE_IP);
    pxDev->ucBurstRate = RTNET_MIN(pxFrame->ucBurstRate, pxDev->ucBurstRate);
    /* Process data */
    xReturn = xHalRTcfgProcessStage1Data(pxFrame->pucCfgData,
                                         rtnet_ntohs(pxFrame->usCfgLen));
    break;
  default:
    return pdFAIL;
  }
  if(xReturn == pdPASS){
    xReturn = prvTxNewAnnouncement(pxDev);
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxNewAnnouncement(xRTcfgDev_t             *pxDev,
                                 xRTcfgNewAnnouncement_t *pxFrame,
                                 uint8_t                 *pucSource)
{
  switch(pxFrame->ucAddrType){
    case rtcfgADDR_MAC:
      if(xRTarpAdd(&pxDev->xArp, pucSource, rtarpWITHOUT_IP) == pdFAIL)
        return pdFAIL;
      break;
    case rtcfgADDR_IP:
      {
        uint32_t ulAddr;
        memcpy(&ulAddr, pxFrame->pucClientAddr, sizeof(uint32_t));
        //ulAddr = rtnet_ntohl(ulAddr);
        if(xRTarpAdd(&pxDev->xArp, pucSource, ulAddr) == pdFAIL) return pdFAIL;
      }
      break;
    default:
      return pdFAIL;
  }
  return prvTxReplyAnnouncement(pxDev, pucSource);
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxReplyAnnouncement(xRTcfgDev_t               *pxDev,
                                     xRTcfgReplyAnnouncement_t *pxFrame,
                                     uint8_t                   *pucSource)
{
  switch(pxFrame->ucAddrType){
    case rtcfgADDR_MAC:
      if(xRTarpAdd(&pxDev->xArp, pucSource, rtarpWITHOUT_IP) == pdFAIL)
        return pdFAIL;
      break;
    case rtcfgADDR_IP:
      {
        uint32_t ulAddr;
        memcpy(&ulAddr, pxFrame->pucClientAddr, sizeof(uint32_t));
        //ulAddr = rtnet_ntohl(ulAddr);
        if(xRTarpAdd(&pxDev->xArp, pucSource, ulAddr) == pdFAIL) return pdFAIL;
      }
      pxFrame = (xRTcfgReplyAnnouncement_t *)
        (((uint8_t *)pxFrame) + rtcfgADDRSIZE_IP);
      break;
    default:
      return pdFAIL;
  }
  if(pxFrame->ucFlags & rtcfgFLAG_READY)
    return xRTarpSetState(&pxDev->xArp, pucSource, eRTarpStateReady);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxInitial(xRTcfgDev_t     *pxDev,
                           xRTcfgInitial_t *pxFrame,
                           uint32_t         ulLen,
                           uint8_t         *pucSource)
{
  if(pxFrame->ucFlags & rtcfgFLAG_READY)
    if(xRTarpSetState(&pxDev->xArp, pucSource, eRTarpStateReady) == pdFAIL)
      return pdFAIL;
  //rtnet_ntohl(pxFrame->ulActiveStation);
  pxDev->usHeartbeat = rtnet_ntohs(pxFrame->usHeartbeat);
  if(pxDev->usHeartbeat > 0){
    if(xRTnetSendEvent(eRTnetEventStartHeartbeat,
                       NULL,
                       0,
                       portMAX_DELAY) == pdFAIL) return pdFAIL;
  }
  pxDev->ulStage2Len = rtnet_ntohl(pxFrame->ulCfgLen);
  if(pxDev->ulStage2Len == 0){
    pxDev->eState = eRTcfgStateReady;
    if(prvTxAcknowledge(pxDev) == pdFAIL){
      if(xRTnetSendEvent(eRTnetEventSendAcknowledge,
                         NULL,
                         0,
                         portMAX_DELAY) == pdFAIL) return pdFAIL;
    }
    if(prvTxReady(pxDev) == pdFAIL){
      return xRTnetSendEvent(eRTnetEventSendReady, NULL, 0, portMAX_DELAY);
    }
    return pdPASS;
  }
  {
    uint32_t ulDataLen, ulProcLen;
    ulDataLen = ulLen - sizeof(xRTcfgInitial_t);
    ulProcLen = xHalRTcfgInitStage2Data(pxFrame->pucCfgData,
                                        ulDataLen,
                                        pxDev->ulStage2Len);
    pxDev->ulStage2Data = ulProcLen;
    if(ulProcLen != ulDataLen){
      pxDev->ucStage2Frame = 0;
      if(prvTxAcknowledge(pxDev) == pdFAIL){
        return xRTnetSendEvent(eRTnetEventSendAcknowledge,
                               NULL,
                               0,
                               portMAX_DELAY);
      }
      return pdPASS;
    }
  }
  pxDev->ucStage2Frame++;
  if(pxDev->ulStage2Data >= pxDev->ulStage2Len){
    pxDev->eState = eRTcfgStateReady;
    if(prvTxAcknowledge(pxDev) == pdFAIL){
      if(xRTnetSendEvent(eRTnetEventSendAcknowledge,
                         NULL,
                         0,
                         portMAX_DELAY) == pdFAIL) return pdFAIL;
    }
    if(prvTxReady(pxDev) == pdFAIL){
      return xRTnetSendEvent(eRTnetEventSendReady, NULL, 0, portMAX_DELAY);
    }
    return pdPASS;
  }
  if(pxDev->ucStage2Frame >= pxDev->ucBurstRate){
    pxDev->ucStage2Frame = 0;
    if(prvTxAcknowledge(pxDev) == pdFAIL){
      return xRTnetSendEvent(eRTnetEventSendAcknowledge,
                             NULL,
                             0,
                             portMAX_DELAY);
    }
    return pdPASS;
  }
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxSubsequent(xRTcfgDev_t        *pxDev,
                              xRTcfgSubsequent_t *pxFrame,
                              uint32_t            ulLen)
{
  uint32_t ulDataLen, ulProcLen;

  ulDataLen = ulLen - sizeof(xRTcfgSubsequent_t);
  ulProcLen = xHalRTcfgProcessStage2Data(pxFrame->pucCfgData,
                                         ulDataLen,
                                         rtnet_ntohl(pxFrame->ulOffset));
  pxDev->ulStage2Data += ulProcLen;
  if(ulProcLen != ulDataLen){
    pxDev->ucStage2Frame = 0;
    if(prvTxAcknowledge(pxDev) == pdFAIL){
      return xRTnetSendEvent(eRTnetEventSendAcknowledge,
                             NULL,
                             0,
                             portMAX_DELAY);
    }
    return pdPASS;
  }
  pxDev->ucStage2Frame++;
  if(pxDev->ulStage2Data >= pxDev->ulStage2Len){
    pxDev->eState = eRTcfgStateReady;
    if(prvTxAcknowledge(pxDev) == pdFAIL){
      if(xRTnetSendEvent(eRTnetEventSendAcknowledge,
                         NULL,
                         0,
                         portMAX_DELAY) == pdFAIL) return pdFAIL;
    }
    if(prvTxReady(pxDev) == pdFAIL){
      return xRTnetSendEvent(eRTnetEventSendReady, NULL, 0, portMAX_DELAY);
    }
    return pdPASS;
  }
  if(pxDev->ucStage2Frame >= pxDev->ucBurstRate){
    pxDev->ucStage2Frame = 0;
    if(prvTxAcknowledge(pxDev) == pdFAIL){
      return xRTnetSendEvent(eRTnetEventSendAcknowledge,
                             NULL,
                             0,
                             portMAX_DELAY);
    }
    return pdPASS;
  }
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxAcknowledge(xRTcfgDev_t         *pxDev,
                               xRTcfgAcknowledge_t *pxFrame,
                               uint8_t             *pucSource)
{
  /* TODO: Implement if master functionality will be supported */
  (void) pxDev;
  (void) pxFrame;
  (void) pucSource;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxReady(xRTcfgDev_t   *pxDev,
                         xRTcfgReady_t *pxFrame,
                         uint8_t       *pucSource)
{
  (void) pxFrame;
  return xRTarpSetState(&pxDev->xArp, pucSource, eRTarpStateReady);
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxHeartbeat(xRTcfgDev_t       *pxDev,
                             xRTcfgHeartbeat_t *pxFrame,
                             uint8_t           *pucSource)
{
  /* TODO: Implement if master functionality will be supported */
  (void) pxDev;
  (void) pxFrame;
  (void) pucSource;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxDeadStation(xRTcfgDev_t         *pxDev,
                               xRTcfgDeadStation_t *pxFrame)
{
  BaseType_t  xReturn = pdPASS;

  switch(pxFrame->ucAddrType){
    case rtcfgADDR_MAC:
      xReturn = xRTarpDelMac(&pxDev->xArp, pxFrame->pucPhyClientAddr);
      break;
    case rtcfgADDR_IP:
      {
        uint32_t ulAddr;
        memcpy(&ulAddr, pxFrame->pucLogClientAddr, sizeof(uint32_t));
        //ulAddr = rtnet_ntohl(ulAddr);
        xReturn = xRTarpDelIp(&pxDev->xArp, ulAddr);
      }
      break;
    default:
      return pdFAIL;
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTxNewAnnouncement(xRTcfgDev_t *pxDev)
{
  xRTcfgNewAnnouncement_t *pxFrame = NULL;
  BaseType_t             xReturn  = pdPASS;
  uint32_t                 ulLen;
  uint8_t                 *pucFrame;

  switch(pxDev->eAddr){
  case eRTcfgAddrMAC:
    /* Prepare Ethernet frame*/
    pucFrame = pucRTnetPrepareFrame(&ulLen,
                                    sizeof(xRTcfgNewAnnouncement_t),
                                    pucRTnetMacBroadcast(),
                                    eRTnetFrameRTcfg);
    if(pucFrame == NULL) return pdFAIL;
    /* Prepare RTcfg frame */
    pxFrame = (xRTcfgNewAnnouncement_t*)(pucFrame + ulLen);
    pxFrame->xHead.ucId = rtcfgFRAME_ID_NEW_ANNOUNCEMENT & rtcfgFRAME_ID_MASK;
    pxFrame->ucAddrType = rtcfgADDR_MAC;
    break;
  case eRTcfgAddrIP:
    /* Prepare Ethernet frame*/
    pucFrame =
        pucRTnetPrepareFrame(&ulLen,
                             sizeof(xRTcfgNewAnnouncement_t) + rtcfgADDRSIZE_IP,
                             pucRTnetMacBroadcast(),
                             eRTnetFrameRTcfg);
    if(pucFrame == NULL) return pdFAIL;
    /* Prepare RTcfg frame */
    pxFrame = (xRTcfgNewAnnouncement_t*)(pucFrame + ulLen);
    /* Update length value */
    ulLen += rtcfgADDRSIZE_IP;
    pxFrame->xHead.ucId = rtcfgFRAME_ID_NEW_ANNOUNCEMENT & rtcfgFRAME_ID_MASK;
    pxFrame->ucAddrType = rtcfgADDR_IP;
    {
      uint32_t addr;
      //addr = rtnet_htonl(pxDev->ulIpAddr);
      addr = pxDev->ulIpAddr;
      memcpy(pxFrame->pucClientAddr, &addr, sizeof(uint32_t));
    }
    pxFrame = (xRTcfgNewAnnouncement_t *)
        (((uint8_t *)pxFrame) + rtcfgADDRSIZE_IP);
    break;
  }
  if(pxDev->eState == eRTcfgStateReady)
    pxFrame->ucFlags = rtcfgFLAG_READY;
  else if(xHalRTcfgRequestsStage2Data() == pdTRUE)
    pxFrame->ucFlags = rtcfgFLAG_STAGE2_DATA;
  pxFrame->ucBurstRate = pxDev->ucBurstRate;
  xReturn = xRTnetSendFrame(pucFrame,
                              ulLen + sizeof(xRTcfgNewAnnouncement_t),
                              portMAX_DELAY);
  if(xReturn == pdFAIL) rtnet_free(pucFrame);
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTxReplyAnnouncement(xRTcfgDev_t *pxDev,
                                     uint8_t     *pucMac)
{
  xRTcfgReplyAnnouncement_t *pxFrame = NULL;
  BaseType_t               xReturn  = pdPASS;
  uint32_t                   ulLen;
  uint8_t                   *pucFrame;

  switch(pxDev->eAddr){
  case eRTcfgAddrMAC:
    /* Prepare Ethernet frame*/
    pucFrame = pucRTnetPrepareFrame(&ulLen,
                                    sizeof(xRTcfgReplyAnnouncement_t),
                                    pucMac,
                                    eRTnetFrameRTcfg);
    if(pucFrame == NULL) return pdFAIL;
    /* Prepare RTcfg frame */
    pxFrame = (xRTcfgReplyAnnouncement_t*)(pucFrame + ulLen);
    pxFrame->xHead.ucId = rtcfgFRAME_ID_REPLY_ANNOUNCEMENT & rtcfgFRAME_ID_MASK;
    pxFrame->ucAddrType = rtcfgADDR_MAC;
    break;
  case eRTcfgAddrIP:
    /* Prepare Ethernet frame*/
    pucFrame =
        pucRTnetPrepareFrame(&ulLen,
                             sizeof(xRTcfgReplyAnnouncement_t) + rtcfgADDRSIZE_IP,
                             pucMac,
                             eRTnetFrameRTcfg);
    if(pucFrame == NULL) return pdFAIL;
    /* Prepare RTcfg frame */
    pxFrame = (xRTcfgReplyAnnouncement_t*)(pucFrame + ulLen);
    /* Update length value */
    ulLen += rtcfgADDRSIZE_IP;
    pxFrame->xHead.ucId = rtcfgFRAME_ID_REPLY_ANNOUNCEMENT & rtcfgFRAME_ID_MASK;
    pxFrame->ucAddrType = rtcfgADDR_IP;
    {
      uint32_t addr;
      //addr = rtnet_htonl(pxDev->ulIpAddr);
      addr = pxDev->ulIpAddr;
      memcpy(pxFrame->pucClientAddr, &addr, sizeof(uint32_t));
    }
    pxFrame = (xRTcfgReplyAnnouncement_t *)
        (((uint8_t *)pxFrame) + rtcfgADDRSIZE_IP);
    break;
  }
  if(pxDev->eState == eRTcfgStateReady)
    pxFrame->ucFlags = rtcfgFLAG_READY;
  pxFrame->ucPadding = 0;
  xReturn = xRTnetSendFrame(pucFrame,
                              ulLen + sizeof(xRTcfgReplyAnnouncement_t),
                              portMAX_DELAY);
  if(xReturn == pdFAIL) rtnet_free(pucFrame);
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTxAcknowledge(xRTcfgDev_t *pxDev)
{
  xRTcfgAcknowledge_t *pxFrame = NULL;
  BaseType_t         xReturn  = pdPASS;
  uint32_t             ulLen;
  uint8_t             *pucFrame;

  /* Prepare Ethernet frame*/
  pucFrame = pucRTnetPrepareFrame(&ulLen,
                                  sizeof(xRTcfgAcknowledge_t),
                                  pxDev->pucMaster,
                                  eRTnetFrameRTcfg);
  if(pucFrame == NULL) return pdFAIL;
  /* Prepare RTcfg frame */
  pxFrame = (xRTcfgAcknowledge_t*)(pucFrame + ulLen);
  pxFrame->xHead.ucId = rtcfgFRAME_ID_ACKNOWLEDGE & rtcfgFRAME_ID_MASK;
  pxFrame->ulAckLen   = rtnet_htonl(pxDev->ulStage2Data);
  xReturn = xRTnetSendFrame(pucFrame,
                            ulLen + sizeof(xRTcfgAcknowledge_t),
                            portMAX_DELAY);
  if(xReturn == pdFAIL) rtnet_free(pucFrame);
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTxReady(xRTcfgDev_t *pxDev)
{
  xRTcfgReady_t *pxFrame = NULL;
  BaseType_t   xReturn  = pdPASS;
  uint32_t       ulLen;
  uint8_t       *pucFrame;

  (void) pxDev;
  /* Prepare Ethernet frame*/
  pucFrame = pucRTnetPrepareFrame(&ulLen,
                                  sizeof(xRTcfgReady_t),
                                  pucRTnetMacBroadcast(),
                                  eRTnetFrameRTcfg);
  if(pucFrame == NULL) return pdFAIL;
  /* Prepare RTcfg frame */
  pxFrame = (xRTcfgReady_t*)(pucFrame + ulLen);
  pxFrame->xHead.ucId = rtcfgFRAME_ID_READY & rtcfgFRAME_ID_MASK;
  xReturn = xRTnetSendFrame(pucFrame,
                            ulLen + sizeof(xRTcfgReady_t),
                            portMAX_DELAY);
  if(xReturn == pdPASS){
    xReturn = xRTnetSendEvent(eRTnetEventReady,
                              NULL,
                              0,
                              portMAX_DELAY);
  }
  else rtnet_free(pucFrame);
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTxHeartbeat(xRTcfgDev_t *pxDev)
{
  xRTcfgHeartbeat_t *pxFrame = NULL;
  BaseType_t       xReturn  = pdPASS;
  uint32_t           ulLen;
  uint8_t           *pucFrame;

  (void) pxDev;
  /* Prepare Ethernet frame*/
  pucFrame = pucRTnetPrepareFrame(&ulLen,
                                  sizeof(xRTcfgHeartbeat_t),
                                  pxDev->pucMaster,
                                  eRTnetFrameRTcfg);
  if(pucFrame == NULL) return pdFAIL;
  /* Prepare RTcfg frame */
  pxFrame = (xRTcfgHeartbeat_t*)(pucFrame + ulLen);
  pxFrame->xHead.ucId = rtcfgFRAME_ID_HEARTBEAT & rtcfgFRAME_ID_MASK;
  xReturn = xRTnetSendFrame(pucFrame,
                            ulLen + sizeof(xRTcfgHeartbeat_t),
                            portMAX_DELAY);
  if(xReturn == pdFAIL) rtnet_free(pucFrame);
  return xReturn;
}
/*---------------------------------------------------------------------------*/
