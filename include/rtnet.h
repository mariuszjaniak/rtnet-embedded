/*****************************************************************************
 *                                                                           *
 *   rtnet.h                                                                 *
 *                                                                           *
 *   RTnet -- Real Time networking                                           *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 * TODO: Implement own list structure (similar to FreeRTOS list) to handle
 *       socket data buffers and TDMA TX queue.
 *****************************************************************************/

#ifndef _RTNET_H_
#define _RTNET_H_

#include <stdint.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"

/* RTnet includes */
#include "rtnet_common.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

 /*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize RTnet stack.
 *
 * \param[in] pucMac  The array containing device Ethernet MAC address.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetInit(uint8_t pucMac[]);

/*****************************************************************************
 *
 * Create a real-time socket.
 *
 * \param[in] ulDomain    Specifies a communication domain, this selects the
 *                        protocol family which will be used for communication.
 * \param[in] ulType      Specifies a communication semantics.
 * \param[in] ulProtocol  Specifies a particular protocol to be used with the
 *                        socket.
 *
 * \return Returns the real-time socket handler if succeed, NULL in other case.
 *
 *****************************************************************************/

xRTnetSocket_t xRTnetSocket(uint32_t ulDomain,
                            uint32_t ulType,
                            uint32_t ulProtocol);

/*****************************************************************************
 *
 * Close a socket.
 *
 * \param[in] xSocket  Socket handler.
 *
 * \return Returns 0 if succeed, else
 *           RTNET_SOCKET_ERROR -- xSocket is invalid socket handler.
 *
 *****************************************************************************/

BaseType_t  xRTnetClosesocket(xRTnetSocket_t xSocket);

/*****************************************************************************
 *
 * Receive a message from a socket.
 *
 * \param[in]  xSocket        Socket handler.
 * \param[out] pvBuf          In case of standard mode, points to buffer where
 *                            received data will be stored or in zero copy mode
 *                            points to buffer pointer where buffer addres
 *                            containning received data will be stored.
 * \param[in]  xLen           Data buffer size.
 * \param[in]  ulFlags        The flags parameter is formed by an OR operation
 *                            on one or more of the following:
 *                              RTNET_ZERO_COPY -- zero copy mode.
 * \param[out] pxSrcAddr      If not NULL, points to buffer where source
 *                            address of the messagge is filled in.
 * \param[in/out] pulAddrlen  Points to the varaible containning size of the
 *                            pxSrcAddr buffer, and where new size will be
 *                            stored (CAUTION: Not supported in current
 *                            implementation).
 *
 * \return Returns the number of bytes received if succeed, else
 *           RTNET_SOCKET_ERROR -- The xSocket is invalid socket handler.
 *           RTNET_EINVAL       -- Socket is not bound.
 *           RTNET_EWOULDBLOCK  -- The socket is marked non-blocking and the
 *                                 requested operation would block.
 *           RTNET_TIMEOUT      -- Reception timeout.
 *
 *****************************************************************************/

int32_t lRTnetRecvfrom(xRTnetSocket_t    xSocket,
                       void             *pvBuf,
                       size_t            xLen,
                       uint32_t          ulFlags,
                       xRTnetSockAddr_t *pxSrcAddr,
                       uint32_t         *pulAddrlen);

/*****************************************************************************
 *
 * Send a message on a socket.
 *
 * \param[in] xSocket    Socket handler.
 * \param[in] pvBuf      Points to buffer containing data.
 * \param[in] xLen       Data size.
 * \param[in] ulFlags    The flags parameter is formed by an OR operation
 *                       on one or more of the following:
 *                         RTNET_ZERO_COPY -- zero copy mode.
 * \param[in] pxDestAddr Points to buffer containing address of the
 *                       target.
 * \param[in] ulAddrlen  A size of the pxDestAddr buffer. (CAUTION: Not
 *                       supported in current implementation).
 *
 * \return Returns the number of bytes transmitted if succeed, else
 *           RTNET_SOCKET_ERROR  -- the xSocket is invalid socket handler.
 *           RTNET_EINVAL        -- Socket is not bound.
 *           RTNET_EMSGSIZE      -- The data size is to large.
 *           RTNET_EADDRNOTAVAIL -- Unknown destination addres.
 *           RTNET_ENOMEM        -- Out of memory.
 *           RTNET_EWOULDBLOCK   -- The socket is marked non-blocking and the
 *                                  requested operation would block.
 *           RTNET_TIMEOUT       -- Transmition timeout.
 *
 *****************************************************************************/

int32_t lRTnetSendto(xRTnetSocket_t    xSocket,
                     void             *pvBuf,
                     size_t            xLen,
                     uint32_t          ulFlags,
                     xRTnetSockAddr_t *pxDestAddr,
                     uint32_t          ulAddrlen);

/*****************************************************************************
 *
 * Bind a name to a socket.
 *
 * \param[in] xSocket    Socket handler.
 * \param[in] pxAddr     Points to buffer containing address that will be
 *                       assigend to the socket.
 * \param[in] ulAddrlen  A size of the pxAddr buffer. (CAUTION: Not
 *                       supported in current implementation).
 *
 * \return Returns zero if succeed, else
 *           RTNET_SOCKET_ERROR  -- The xSocket is invalid socket handler.
 *           RTNET_EINVAL        -- Socket is already bound.
 *           RTNET_EADDRNOTAVAIL -- Invalid addres.
 *           RTNET_EADDRINUSE    -- Address is already bound to other socket
 *           RTNET_ENOMEM        -- Out of memory.
 *
 *****************************************************************************/

BaseType_t  xRTnetBind(xRTnetSocket_t          xSocket,
                         const xRTnetSockAddr_t *pxAddr,
                         uint32_t                ulAddrlen);

/*****************************************************************************
 *
 * Set options on sockets
 *
 *  \param[in] xSocket   Socket handler.
 *  \param[in] lLevel    A level at which the option resides. (CAUTION: Not
 *                       supported in current implementation).
 *  \param[in] lOptname  A option name.
 *  \param[in] pvOptval  Points to the buffer containing a new option value.
 *  \param[in] xOptlen   A size of the pvOptval buffer. (CAUTION: Not
 *                       supported in current implementation).
 *
 *  \return  Returns zero if succeed, else
 *             RTNET_ENOPROTOOPT -- Unknown option name.
 *
 *****************************************************************************/

BaseType_t  xRTnetSetsockopt(xRTnetSocket_t  xSocket,
                               int32_t         lLevel,
                               int32_t         lOptname,
                               const void     *pvOptval,
                               size_t          xOptlen);

/*****************************************************************************
 *
 * Add new entry to RTnet routing table.
 *
 * \param[in] pucMac    The array containing device Ethernet MAC address.
 * \param[in] ulIpAddr  The IP address (network byte order).
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetRouteAdd(uint8_t pucMac[], uint32_t ulIpAddr);

/*****************************************************************************
 *
 * Remove MAC address from RTnet routing table.
 *
 * \param[in] pucMac  The array containing device Ethernet MAC address.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetRouteDelMac(uint8_t pucMac[]);

/*****************************************************************************
 *
 * Remove IP address from RTnet routing table.
 *
 * \param[in] ulIpAddr  The IP address (network byte order).
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetRouteDelIp(uint32_t ulIpAddr);

/*****************************************************************************
 *
 * Get device IP address.
 *
 * \return Returns the IP address of the device (network byte order).
 *
 *****************************************************************************/

uint32_t ulRTnetGetIpAddr(void);

/*****************************************************************************
 *
 * Request a UDP payload buffer (required in zero copy mode when transmition).
 *
 * \param[in] xSize  The requested size of the UDP payload buffer.
 *
 * \return Returns a pointer to the UDP payload buffer if succeed, NULL in
 *         other case.
 *****************************************************************************/

void *pvRTnetGetUdpDataBuffer(size_t xSize);

/*****************************************************************************
 *
 * Release a UDP payload buffer (required in zero copy mode when reception).
 *
 * \param[in] pvData  Points to the UDP payload buffer.
 *
 *****************************************************************************/

void vRTnetReleaseUdpDataBuffer(void *pvData);

/*****************************************************************************
 * Block until RTnet stack will be ready.
 *
 * \param[in] xTicksToWait The maximum amount of time the task should block
 *                         waiting RTnet stack will be ready. The call will
 *                         return immediately if this is set to 0. The time is
 *                         defined in tick periods so the constant
 *                         portTICK_RATE_MS should be used to convert to real
 *                         time if this is required.
 *
 * \return Returns pdPASS if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetWaitReady(portTickType xTicksToWait);

/*****************************************************************************
 * Block until next TDMA cycle.
 *
 * \param[in] xTicksToWait The maximum amount of time the task should block
 *                         waiting RTnet stack will be ready. The call will
 *                         return immediately if this is set to 0. The time is
 *                         defined in tick periods so the constant
 *                         portTICK_RATE_MS should be used to convert to real
 *                         time if this is required.
 *
 * \return Returns pdPASS if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTnetWaitSync(portTickType xTicksToWait);

/*****************************************************************************
 *
 * Update network devices link status, enable Rx/Tx if link is UP, disable if
 * link is down.
 *
 * \return Returns the pdTRUE value if link is UP, pdFAIL if link is down.
 *
 *****************************************************************************/

BaseType_t  xRTnetUpdateLinkStatus(void);

#endif /* _RTNET_H_ */
