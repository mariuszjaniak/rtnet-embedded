/*****************************************************************************
 *                                                                           *
 *   halNetworkInterface.h                                                   *
 *                                                                           *
 *   Network interface hardware abstraction layer                            *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _HAL_NETWORK_INTERFACE_H
#define _HAL_NETWORK_INTERFACE_H

#include <stdint.h>
#include "FreeRTOS.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize hardware network devices.
 *
 * \param[in] pucMac  The array containing Ethernet MAC address.
 * \param[in] pxRTnet Points to the RTnet structure.
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xHalNetworkInterfaceInitialise(uint8_t pucMac[]);

/*****************************************************************************
 *
 * Send the Ethernet frame through selected network device.
 *
 * \param[in] pucBuffer     The array containing Ethernet frame to send.
 * \param[in] xDataLength   The length of the Ethernet frame.
 * \param[in] pullTimeStamp Points to the place in Ethernet frame where
 *                          transmission time stamp will be stored (if NULL no
 *                          time stamp will be stored in the frame).
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xHalNetworkInterfaceOutput(uint8_t   pucBuffer[],
                                       size_t    xDataLength,
                                       uint64_t *pullTimeStamp);

/*****************************************************************************
 *
 * Update network devices link status, enable Rx/Tx if link is UP, disable if
 * link is down.
 *
 * \return Returns the pdTRUE value if link is UP, pdFAIL if link is down.
 *
 *****************************************************************************/

BaseType_t xHalNetworkInterfaceCheckLinkStatus(void);

#endif /* _HAL_NETWORK_INTERFACE_H */

