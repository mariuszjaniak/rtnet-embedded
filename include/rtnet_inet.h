/*****************************************************************************
 *                                                                           *
 *   rtnet_inet.h                                                            *
 *                                                                           *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTNET_INET_H_
#define _RTNET_INET_H_

#include <stdint.h>

#include "RTnetConfig.h"

#ifdef rtnetconfigBYTE_ORDER_LITTLE_ENDIAN
  uint64_t rtnet_htonll(uint64_t hostlonglong);
  uint32_t rtnet_htonl(uint32_t hostlong);
  uint16_t rtnet_htons(uint16_t hostshort);
  uint64_t rtnet_ntohll(uint64_t netlonglong);
  uint32_t rtnet_ntohl(uint32_t netlong);
  uint16_t rtnet_ntohs(uint16_t netshort);
#else
  #define rtnet_htonll(x) (x)
  #define rtnet_ntohll(x) (x)
  #define rtnet_htonl(x)  (x)
  #define rtnet_ntohl(x)  (x)
  #define rtnet_htons(x)  (x)
  #define rtnet_ntohs(x)  (x)
#endif

#endif /* _RTNET_INET_H_ */
