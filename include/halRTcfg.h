/*****************************************************************************
 *                                                                           *
 *   halRTcfg.c                                                              *
 *                                                                           *
 *   RTcfg hardware abstraction layer                                        *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _HAL_RTCFG_SERVICE_H
#define _HAL_RTCFG_SERVICE_H

#include "FreeRTOS.h"

#include "rtcfg.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Process configuration stage 1 data.
 *
 * \param[in] pucData  Points to array containing stage 1 data.
 * \param[in] usLen    Data length.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xHalRTcfgProcessStage1Data(uint8_t  *pucData,
                                         uint16_t  usLen);

/*****************************************************************************
 *
 * Initialize configuration stage 2.
 *
 * \param[in] pucData      Points to array containing stage 2 data.
 * \param[in] ulLen        Data length.
 * \param[in] ulStage2Len  The state 2 data length.
 *
 * \return Returns the number of proccessed data.
 *
 *****************************************************************************/

uint32_t xHalRTcfgInitStage2Data(uint8_t  *pucData,
                                 uint32_t  ulLen,
                                 uint32_t  ulStage2Len);

/*****************************************************************************
 *
 * Process configuration stage 2 data.
 *
 * \param[in] pucData  Points to array containing stage 2 data.
 * \param[in] ulLen    Data length.
 * \param[in] ulOffset Data offset.
 *
 * \return Returns the number of proccessed data.
 *
 *****************************************************************************/

uint32_t xHalRTcfgProcessStage2Data(uint8_t  *pucData,
                                    uint32_t  ulLen,
                                    uint32_t  ulOffset);

/*****************************************************************************
 *
 * Determine if slave should request satage 2 configuration data from master.
 *
 * \return Returns the pdTRUE value if slave is not configuret yet and needs
 *         configuration data from master, pdFALSE in other case.
 *
 *****************************************************************************/

BaseType_t  xHalRTcfgRequestsStage2Data(void);

#endif /* _HAL_RTCFG_SERVICE_H */

