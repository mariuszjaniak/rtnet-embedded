/*****************************************************************************
 *                                                                           *
 *   tdmaCfg.h                                                               *
 *                                                                           *
 *   TDMA discipline configuration service                                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _TDMACFG_H_
#define _TDMACFG_H_

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* RTnet includes */
#include "RTnetConfig.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define tdmacfgMIN_SLOT_SIZE (60)
#define tdmacfgMAX_SLOT_SIZE (rtnetconfigETHERNET_MTU_SIZE)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xTDMA_SLOT{
  uint64_t      ullOffset;
  uint32_t      ulPeriod;
  uint32_t      ulPhasing;
  uint32_t      ulSize;
  uint32_t      ulId;
};
typedef struct xTDMA_SLOT xTdmaSlot_t;

struct xTDMA_CFG{
  xTdmaSlot_t pxSlots[rtnetconfigTDMA_SLOTS_NUMBER];
  uint64_t    ullCycle;
  uint32_t    ulLen;
  uint32_t    ulMaxSize;
};
typedef struct xTDMA_CFG xTdmaCfg_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize TDMA configuration service and register configuration structure.
 *
 * \param[in] pxCfg  Points to the TDMA configuration structure.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaCfgInit(xTdmaCfg_t *pxCfg);

/*****************************************************************************
 *
 * Add new slot to TDMA configuration.
 *
 * \param[in] pxSlot  Points to the slot structure.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaCfgAdd(xTdmaSlot_t *pxSlot);

/*****************************************************************************
 *
 * Proccess 'tdmacfg' command (see RTnet documentation for details)
 *
 * \param[in] argc  Number of command arguments.
 * \param[in] argv  Points to array containing command line arguments.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaCfgCmd(int argc, char *argv[]);

/*****************************************************************************
 *
 * Determine if TDMA discipline is configured and has asigend slots.
 *
 * \return Returns the pdPASS value if TDMA is confiured, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaCfgHasSlot(void);

#endif /* _TDMACFG_H_ */
