/*****************************************************************************
 *                                                                           *
 *   rtudp.h                                                                 *
 *                                                                           *
 *   UDP -- real-time User Datagram Protocol                                 *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTUDP_H_
#define _RTUDP_H_

#include <stddef.h>
#include <stdint.h>

#include "FreeRTOS.h"

/* RTnet includes */
#include "rtnet_common.h"
#include "rtbuf.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtudpIPPROTOCOL 0x11

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Process UDP over IPv4 frame.
 *
 * \param[in] pxBuf    Points to a real-time data buffer.
 * \param[in] pxSaddr  Points to the buffer containing source address.
 * \param[in] pxDaddr  Points to the buffer containing destination address.
 *
 * \return Returns pdPASS if suceed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xRTudpProcessIPv4Frame(xRTbuf_t *pxBuf,
                                     uint32_t  ulSaddr,
                                     uint32_t  ulDaddr);

/*****************************************************************************
 *
 * Prepare UDP frame.
 *
 * \param[out] pucFrame  Points to the frame buffer.
 * \param[in]  pxSrcAddr Points to the buffer containing source address.
 * \param[in]  pxDstAddr Points to the buffer containing destination address.
 * \param[in]  pucData   Points to the buffer containing UDP payload.
 * \param[in]  usLen     The size of the UDP payload.
 * \param[in]  ucOpt     The option parameter is formed by an OR operation
 *                       on one or more of the following:
 *                         RTNET_SO_UDPCKSUM -- Add checksum to UDP header.
 *
 * \return Returns frame size.
 *
 *****************************************************************************/

uint32_t ulRTudpFrame(uint8_t          *pucFrame,
                      xRTnetSockAddr_t *pxSrcAddr,
                      xRTnetSockAddr_t *pxDstAddr,
                      uint8_t          *pucData,
                      uint16_t          usLen,
                      uint8_t           ucOpt);


/*****************************************************************************
 *
 * Get size of UDP frame containing given payload.
 *
 * \param[in] xLen  The size of the UDP payload.
 *
 * \return Returns the UDP frame size.
 *
 *****************************************************************************/

size_t xRTudpSize(size_t xLen);

/*****************************************************************************
 *
 * Get size of UDP frame header.
 *
 * \return Returns the header size.
 *
 *****************************************************************************/

size_t xRTudpHeaderSize(void);

#endif /* _RTUDP_H_ */
