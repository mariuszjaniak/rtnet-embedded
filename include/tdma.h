/*****************************************************************************
 *                                                                           *
 *   tdma.h                                                                  *
 *                                                                           *
 *   TDMA (Time Division Multiple Access) RTmac discipline                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _TDMA_H_
#define _TDMA_H_

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* RTnet includes */
#include "tdmaCfg.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define tdmaRTMAC_TYPE 0x0001

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

enum eTDMA_STATE{
  eTdmaStatePreInit,
  eTdmaStateInit,
  eTdmaStateCalibrationRequest,
  eTdmaStateCalibrationReplay,
  eTdmaStateNormal,
};
typedef enum eTDMA_STATE eTdmaState_t;

enum eTDMA_FLAG{
  eTdmaEmpty,
  eTdmaFull,
};
typedef enum eTDMA_FLAG eTdmaFlag_t;

enum eTDMA_FRAME{
  eTdmaFrameSynchronisation,
  eTdmaFrameCalibrationRequest,
  eTdmaFrameCalibrationReply,
};
typedef enum eTDMA_FRAME eTdmaFrame_t;

struct xTDMA_TIME{
  int64_t       llOffs;
  uint64_t      ullTrans;
  uint64_t      ullSched;
  uint64_t      ullProc;
  uint64_t      ullTic;
};
typedef struct xTDMA_TIME xTdmaTime_t;

struct xTDMA_DESC{
  uint64_t     *pullTimeStamp;
  uint8_t      *pucData;
  size_t        xLen;
};
typedef struct xTDMA_DESC xTdmaDesc_t;

struct xTDMA_FIFO{
  xTdmaDesc_t   xBuffer[rtnetconfigTDMA_FIFO_LENGTH];
  uint32_t      ulIn;
  uint32_t      ulOut;
  eTdmaFlag_t   eFlag;
};
typedef struct xTDMA_FIFO xTdmaFifo_t;

struct xTDMA_COUNT{
  uint32_t      ulInit;
  uint32_t      ulCalib;
  uint32_t      ulWait;
  uint32_t      ulSlot;
  uint32_t      ulCycle;
};
typedef struct xTDMA_COUNT xTdmaCount_t;

struct xTDMA_DEV{
  xTdmaFifo_t   xTxFifo;
  xTdmaTime_t   xT;
  xTdmaCount_t  xCount;
  xTdmaCfg_t    xCfg;
  eTdmaState_t  eState;
};
typedef struct xTDMA_DEV xTdmaDev_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize TDMA discipline.
 *
 * \param[in] pxDev  Points to the TDMA discipline structure.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaInit(xTdmaDev_t *pxDev);

/*****************************************************************************
 *
 * Process received TDMA frame.
 *
 * \param[in] pxDev        Points to the TDMA discipline structure.
 * \param[in] pucFrame     The array containing received frame.
 * \param[in] ullTimeStamp The frame time stamp (the time when frame has been
 *                         received).
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaProcessFrame(xTdmaDev_t *pxDev,
                                uint8_t     pucFrame[],
                                uint64_t    ullTimeStamp);

/*****************************************************************************
 *
 * Queue frame for transmission.
 *
 * \param[in] pxDev    Points to the TDMA discipline structure.
 * \param[in] pucFrame The array containing Ethernet frame.
 * \param[in] ulLen    The length of the Ethernet frame.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaSend(xTdmaDev_t *pxDev,
                        uint8_t     pucFrame[],
                        uint32_t    ulLen);

/*****************************************************************************
 *
 * Handle slot event (should be called from timer interrupt service routine or
 * from deferred interrupt handler task).
 *
 * \param[in] pxDev    Points to the TDMA discipline structure.
  *
 * \return Returns the pointer to the Ethernet frame that has been already sent.
 *         If frame has been allocated dynamically, it should be freed as soon
 *         as possible. Return NULL if there was no data to send in this time
 *         slot.
 *
 *****************************************************************************/

uint8_t *pucTdmaProcessSlotEvent(xTdmaDev_t *pxDev);

/*****************************************************************************
 *
 * Post process slot event -- clean meamory, send event to RTnet stack (must
 * not be called from ISR or critical section)
 *
 * \param[in] pxDev    Points to the TDMA discipline structure.
 * \param[in] pucData  Point to dynamically allocated memory buffer which will
 *                     be released.
 *
 * \return Returns the pdPASS value if succeed, pdFAIL in other case.
 *
 *****************************************************************************/

BaseType_t  xTdmaPostProcessSlotEvent(xTdmaDev_t *pxDev, uint8_t *pucData);

/*****************************************************************************
 *
 * Get size of selected TDMA frame.
 *
 * \param[in] eFrameType  A frame type.
 *
 * \return Returns the selected frame size.
 *
 *****************************************************************************/

size_t xTdmaFrameSize(eTdmaFrame_t eFrameType);

/*****************************************************************************
 *
 * Update network devices link status, enable Rx/Tx if link is UP, disable if
 * link is down.
 *
 * \return Returns the pdPASS value if link is UP, pdFAIL if link is down.
 *
 *****************************************************************************/

BaseType_t  xTdmaUpdateLinkStatus(void);

/*****************************************************************************
 *
 * Check if TDMA is ready
 *
 * \param[in] pxDev    Points to the TDMA discipline structure.
 *
 * \return Returns the pdTRUE value if TDMA is ready, pdFALSE otherwise.
 *
 *****************************************************************************/

BaseType_t  xTdmaIsReady(xTdmaDev_t *pxDev);

#endif /* _TDMA_H_ */
