/*****************************************************************************
 *                                                                           *
 *   rtbuf.h                                                                 *
 *                                                                           *
 *   Real-time data buffers                                                  *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef _RTBUF_H_
#define _RTBUF_H_

#include <stdint.h>
#include <stddef.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "list.h"

/* RTnet includes */

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xRTBUF{
  xListItem  xItem;
  uint8_t   *pucData;
  uint32_t   ulIdx;
  uint32_t   ulSize;
};
typedef struct xRTBUF xRTbuf_t;

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *
 * Initialize RTbuf service.
 *
 * \return Returns the pdPASS value if succeed initialization, pdFAIL in
 *         other case.
 *
 *****************************************************************************/

BaseType_t  xRTbufInit(void);

/*****************************************************************************
 *
 * Get RTbuf object with allocated requested memory region.
 *
 * \param[in] xSize        Size of the memory block, in bytes.
 * \param[in] xTicksToWait The maximum amount of time the task should block
 *                         waiting for RTbuf object. The call will return
 *                         immediately if this is set to 0. The time is defined
 *                         in tick periods so the constant portTICK_RATE_MS
 *                         should be used to convert to real time if this is
 *                         required.
 *
 * \return On success, a pointer to the RTbuf objecy. If the function failed, a
 *         NULL pointer is returned.
 *
 *****************************************************************************/

xRTbuf_t *pxRTbufGet(size_t xSize, portTickType xTicksToWait);

/*****************************************************************************
 *
 * Release RTbuf object and associated memory region.
 *
 * \param[in] pxBuf  Points to the RTbuf object.
 *
 *****************************************************************************/

void vRTbufRelease(xRTbuf_t *pxBuf);

#endif /* _RTBUF_H_ */
