/*****************************************************************************
 *                                                                           *
 *   rtnet_inet.c                                                            *
 *                                                                           *
 *   ARM Cortex-M3 specific inet functions                                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include "rtnet_inet.h"

#ifdef rtnetconfigBYTE_ORDER_LITTLE_ENDIAN

extern uint64_t rtnet_swap64(uint64_t x);
extern uint32_t rtnet_swap32(uint32_t x);
extern uint16_t rtnet_swap16(uint16_t x);

uint64_t rtnet_htonll(uint64_t hostlonglong)
{
  return rtnet_swap64(hostlonglong);
}

uint32_t rtnet_htonl(uint32_t hostlong)
{
  return rtnet_swap32(hostlong);
}

uint16_t rtnet_htons(uint16_t hostshort)
{
  return rtnet_swap16(hostshort);
}

uint64_t rtnet_ntohll(uint64_t netlonglong)
{
  return rtnet_swap64(netlonglong);
}

uint32_t rtnet_ntohl(uint32_t netlong)
{
  return rtnet_swap32(netlong);
}

uint16_t rtnet_ntohs(uint16_t netshort)
{
  return rtnet_swap16(netshort);
}

#endif
