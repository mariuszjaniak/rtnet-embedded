/*****************************************************************************
 *                                                                           *
 *   halRTcfg.c                                                              *
 *                                                                           *
 *   RTcfg hardware abstraction layer                                        *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* Standard includes. */
#include <stdint.h>
#include <string.h>

/* RTnet includes */
#include "halRTcfg.h"
#include "tdmaCfg.h"

/* Other includes. */
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

typedef BaseType_t  (*fHalRTcfgCmd)(int argc, char *argv[]);

struct xRTHALCFG_CMD{
  const char *pcName;  /* the command name */
  fHalRTcfgCmd   fun;     /* command function handler */
};
typedef struct xRTHALCFG_CMD xHalRTcfgCmd_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

BaseType_t  prvCmdDummy(int argc, char *argv[]);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

static const xHalRTcfgCmd_t gpxCmd[] = {
  {"$TDMACFG", xTdmaCfgCmd},
  {"ifconfig", prvCmdDummy},
  {NULL, NULL}
};


/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xHalRTcfgProcessStage1Data(uint8_t  *pucData,
                                         uint16_t  usLen)
{
  int   argc, new, procLine, procCmd;
  char *argv[rtnetconfigRTCFG_CMD_ARG_NUM];
  char *line, *ptr;

  argc     = 0;
  new      = 1;
  procLine = 1;
  procCmd  = 1;
  line     = (char*) pucData;
  ptr      = (char*) pucData;

  while(procLine){
    while(procCmd){
      if(ptr - line >= usLen) return pdFAIL;
      else if(*ptr == '\n'){
        *ptr     = '\0';
        procCmd  = 0;
        procLine = 0;
      }
      else if(*ptr == ';'){
        *ptr    = '\0';
        procCmd = 0;
      }
      else if(*ptr == ' '){
        *ptr = '\0';
        new  = 1;
      }
      else{
        if(new){
          if(argc < rtnetconfigRTCFG_CMD_ARG_NUM){
            argv[argc] = ptr;
            argc++;
            new = 0;
          }
          else return pdFAIL;
        }
      }
      ptr++;
    }
    if(argc){
      const xHalRTcfgCmd_t *pxCmd;
      pxCmd = gpxCmd;
      while(pxCmd->pcName){
        if(!strcmp(argv[0], pxCmd->pcName))
          if(pxCmd->fun(argc, argv) == pdFAIL) return pdFAIL;
        pxCmd++;
      }
      argc = 0;
    }
    procCmd = 1;
    new     = 1;
  }
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

uint32_t xHalRTcfgInitStage2Data(uint8_t  *pucData,
                                 uint32_t  ulLen,
                                 uint32_t  ulStage2Len)
{
  return ulLen;
}
/*---------------------------------------------------------------------------*/

uint32_t xHalRTcfgProcessStage2Data(uint8_t  *pucData,
                                    uint32_t  ulLen,
                                    uint32_t  ulOffset)
{
  return ulLen;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalRTcfgRequestsStage2Data(void)
{

  return pdTRUE;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvCmdDummy(int argc, char *argv[])
{
  (void) argc;
  (void) argv;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/
