/*****************************************************************************
 *                                                                           *
 *   halTimerInterface.c                                                     *
 *                                                                           *
 *   The lm3s68962 timer hardware abstraction layer                           *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* RTnet includes */
#include "halTimerInterface.h"

/* Driver includes. */
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_timer.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "driverlib/sysctl.h"

/* Other includes. */
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define timerRELOAD_VALUE 0xFFFFFFFFUL

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xTIMER_DEV{
  xSemaphoreHandle  xTimerEventSemaphore;
  xTdmaDev_t       *pxTdma;
  uint32_t          ulTimeMsb;
  uint32_t          ulNsPerTic;                          /* [ns/tic] */
};
typedef struct xTIMER_DEV xTimerDev_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/* A deferred interrupt handler task that processes received frames. */
static void prvTimerDeferredInterruptHandlerTask(void *pvParameters);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/* Network devices */
static xTimerDev_t xTimer;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xHalTimerInterfaceInitialise(xTdmaDev_t *pxTdma)
{
  BaseType_t  xReturn = pdFAIL;

  /* Initialize timer structure */
  xTimer.pxTdma     = pxTdma;
  xTimer.ulTimeMsb  = 0;
  xTimer.ulNsPerTic = 1000000000UL / gStat.sysClock;
  xTimer.xTimerEventSemaphore = xSemaphoreCreateCounting(1, 0);
  if(xTimer.xTimerEventSemaphore == NULL){
    /* Clean */
    return pdFAIL;
  }
  /* The timer deferred interrupt handler task is created at the highest
   * possible priority to ensure the interrupt handler can return directly to
   * it no matter which task was running when the interrupt occurred. */
  xReturn = xTaskCreate(prvTimerDeferredInterruptHandlerTask,
                        NULL,
                        configSTACK_SIZE_RTNETTIMER,
                        &xTimer,
                        configTASK_PRIO_RTNETTIMER,
                        NULL);
  if(xReturn == pdFAIL){
    /* Clean */
    vSemaphoreDelete(xTimer.xTimerEventSemaphore);
    return pdFAIL;
  }
  /* Enable TIMER0 */
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
  /* Initialize 32-bit periodic timer */
  TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
  /* Enable timer interrupt */
  IntEnable(INT_TIMER0A);
  /* Priorities that allow for calling FreeRTOS API are in range:
   * 3 (the highest) to 7 (the lowest) -- see FreeRTOSConfig.h */
  IntPrioritySet(INT_TIMER0A, configINTERRUPT_PRIORITY_TIMER0);
  /* Chose interrupt event */
  TimerIntEnable(TIMER0_BASE,TIMER_TIMA_TIMEOUT);
  /* Set timer */
  TimerLoadSet(TIMER0_BASE, TIMER_A, timerRELOAD_VALUE);
  /* Enable timer */
  TimerEnable(TIMER0_BASE, TIMER_A);
  /* Enable TIMER1 */
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
  /* Initialize 32-bit one shot timer */
  TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
  /* Enable timer interrupt */
  IntEnable(INT_TIMER1A);
  /* Priorities that allow for calling FreeRTOS API are in range:
   * 3 (the highest) to 7 (the lowest) -- see FreeRTOSConfig.h */
  IntPrioritySet(INT_TIMER1A, configINTERRUPT_PRIORITY_TIMER1);
  /* Chose interrupt event */
  TimerIntEnable(TIMER1_BASE,TIMER_TIMA_TIMEOUT);
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerGet(uint64_t *pullTime)
{
  uint64_t     ullTic;
  uint32_t     ulLsb, ulMsb;

  ulLsb  = timerRELOAD_VALUE - TimerValueGet(TIMER0_BASE, TIMER_A);
  ulMsb  = xTimer.ulTimeMsb;
  ullTic = (((uint64_t) ulMsb) << 32ULL) | ((uint64_t) ulLsb);
  *pullTime = ullTic * xTimer.ulNsPerTic;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerSet(uint64_t ullTime)
{
  uint64_t     ullTic;
  uint32_t     ulLsb;

  ullTic = ullTime / xTimer.ulNsPerTic;
  ulLsb  = (uint32_t) ullTic;
  TimerLoadSet(TIMER0_BASE, TIMER_A, timerRELOAD_VALUE - ulLsb);
  xTimer.ulTimeMsb = (uint32_t)(ullTic >> 32ULL);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalTimerNewEvent(uint64_t ullTime,
                                uint64_t ullLim)
{
  uint64_t     ullActual, ullPeriod;
  uint32_t     ulTic;

  xHalTimerGet(&ullActual);
  //if(ullTime < ullActual) return pdFAIL;
  ullPeriod = ullTime - ullActual;
  if(ullPeriod > ullLim) return pdFAIL;
  ulTic = (uint32_t)(ullPeriod / xTimer.ulNsPerTic);
  TimerLoadSet(TIMER1_BASE, TIMER_A, ulTic);
  TimerEnable(TIMER1_BASE, TIMER_A);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

void TIMER0_isr(void)
{
  /* Clear the timer interrupt */
  TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
  /* Increase MSB */
  ++xTimer.ulTimeMsb;
}
/*---------------------------------------------------------------------------*/

void TIMER1_isr(void)
{
  /* Clear the timer interrupt */
  TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
  /* Unblock the deferred interrupt handler task */
  if(xTimer.pxTdma->eState != eTdmaStatePreInit){
    signed BaseType_t  hptw = FALSE;
    xSemaphoreGiveFromISR(xTimer.xTimerEventSemaphore, &hptw);
    portEND_SWITCHING_ISR(hptw);
  }
}
/*---------------------------------------------------------------------------*/

void prvTimerDeferredInterruptHandlerTask(void *pvParameters)
{
  xTimerDev_t *pxDev = pvParameters;

  for( ;; ){
    uint8_t *pucPtr;
    /* Wait for the timer interrupt */
    while(xSemaphoreTake(pxDev->xTimerEventSemaphore, portMAX_DELAY) ==
        pdFALSE);
    /* Process slot event (in order to reduce jitter move to timer isr) */
    taskENTER_CRITICAL();
    {
      pucPtr = pucTdmaProcessSlotEvent(pxDev->pxTdma);
    }
    taskEXIT_CRITICAL();
    /* Post proces slot event */
    xTdmaPostProcessSlotEvent(pxDev->pxTdma, pucPtr);
  }
}
/*---------------------------------------------------------------------------*/

