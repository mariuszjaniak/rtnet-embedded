/*****************************************************************************
 *                                                                           *
 *   rtnet_struct.h                                                          *
 *                                                                           *
 *   Packed structure port gcc compiler.                                     *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#ifndef RTNET_STRUCT_BEGIN
  #define RTNET_STRUCT_BEGIN()
#endif

#ifndef RTNET_STRUCT_STRUCT
  #define RTNET_STRUCT_STRUCT  __attribute__( (packed) )
#endif

#ifndef RTNET_STRUCT_END
  #define RTNET_STRUCT_END()
#endif













