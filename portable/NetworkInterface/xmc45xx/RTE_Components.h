
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'Relax_USB_HID' 
 * Target:  'XMC4500-F100x1024 Flash' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

//#define RTE_CMSIS_RTOS                  /* CMSIS-RTOS */
//        #define RTE_CMSIS_RTOS_RTX              /* CMSIS-RTOS Keil RTX */
//#define RTE_DEVICE
//#define RTE_DEVICE_STARTUP
//#define RTE_Drivers_USBD
//#define RTE_USB_Core                    /* USB Core */
//#define RTE_USB_Device_0                /* USB Device 0 */
//#define RTE_USB_Device_HID_0            /* USB Device HID instance 0 */

#define RTE_Drivers_ETH_MAC
#define ETH_MAC_TIME_STAMP 1

#endif /* RTE_COMPONENTS_H */
