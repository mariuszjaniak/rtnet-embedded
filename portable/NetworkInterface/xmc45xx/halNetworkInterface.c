/*****************************************************************************
 *                                                                           *
 *   halNetworkInterface.c                                                   *
 *                                                                           *
 *   The XMC4500 network interface hardware abstraction layer                *
 *                                                                           *
 *   Copyright (C) 2015 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                               *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/* Standard includes. */
#include <stdint.h>
#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

/* RTnet includes */
#include "rtnet_inet.h"
#include "rtnet_private.h"
#include "halNetworkInterface.h"
#include "halTimerInterface.h"

/* Driver includes. */
#include <DAVE.h>
#include "xmc_gpio.h"
//#include "xmc_eth_mac.h"
#include "Driver_ETH_MAC.h"
#include "Driver_ETH_PHY.h"
#include "eth1588TimerTune.h"

#include <xmc_ccu8.h>

/* Other includes. */
//#include "types.h"
#include "globals.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define TIME_TO_UINT64(sec, nsec) (((uint64_t)(sec)*1000000000UL) + ((uint64_t)(nsec)))

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xNETWORK_DEV{
  TaskHandle_t        xTaskEthRxEvent;
  uint64_t            ullTimeStamp;
  ARM_DRIVER_ETH_MAC *xMac;
  ARM_DRIVER_ETH_PHY *xPhy;
  ARM_ETH_LINK_STATE  link;
};
typedef struct xNETWORK_DEV xNetworkDev_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static void prvEthernetMacNotify(uint32_t event);
/* A deferred interrupt handler task that processes received frames. */
static void prvETHDeferredInterruptHandlerTask(void *pvParameters);

static void prvTuneEth1588Timer(TickType_t xPeriod, uint32_t ulRepeat);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/* CMISI driver structures */
extern ARM_DRIVER_ETH_MAC Driver_ETH_MAC0;
extern ARM_DRIVER_ETH_PHY Driver_ETH_PHY0;

/* Network devices */
static xNetworkDev_t xNetwork;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xHalNetworkInterfaceInitialise(uint8_t pucMac[])
{
  BaseType_t  xReturn    = pdFAIL;

  xNetwork.xMac = &Driver_ETH_MAC0;
  xNetwork.xPhy = &Driver_ETH_PHY0;
  xNetwork.link = ARM_ETH_LINK_DOWN;
  /* Initialize Media Access Controller */
  xNetwork.xMac->Initialize(prvEthernetMacNotify);
  xNetwork.xMac->PowerControl(ARM_POWER_FULL);
  xNetwork.xMac->SetMacAddress((ARM_ETH_MAC_ADDR*) pucMac);
  {
      ARM_ETH_MAC_TIME xTime;
      xTime.ns  = 0;
      xTime.sec = 0;
      xNetwork.xMac->ControlTimer(ARM_ETH_MAC_TIMER_SET_TIME, &xTime);
    }
  /* Initialize Physical Media Interface */
  if (xNetwork.xPhy->Initialize(xNetwork.xMac->PHY_Read,
                                xNetwork.xMac->PHY_Write) == ARM_DRIVER_OK) {
    xNetwork.xPhy->PowerControl(ARM_POWER_FULL);
    xNetwork.xPhy->SetInterface(ARM_ETH_INTERFACE_RMII);
    xNetwork.xPhy->SetMode(ARM_ETH_PHY_AUTO_NEGOTIATE);
  }
  else{
    xNetwork.xMac->PowerControl(ARM_POWER_OFF);
    return pdFAIL;
  }
  /* The Rx deferred interrupt handler task is created at the highest
   * possible priority (but lower then timer task) to ensure the interrupt
   * handler can return directly to it no matter which task was running when
   * the interrupt occurred. */
  xReturn = xTaskCreate(prvETHDeferredInterruptHandlerTask,
                        NULL,
                        configSTACK_SIZE_RTNETETH,
                        &xNetwork,
                        configTASK_PRIO_RTNETETH,
                        &xNetwork.xTaskEthRxEvent);
  if(xReturn == pdFAIL){
    xNetwork.xMac->PowerControl(ARM_POWER_OFF);
    return pdFAIL;
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xHalNetworkInterfaceOutput(uint8_t   pucBuffer[],
                                       size_t    xDataLength,
                                       uint64_t *pullTimeStamp)
{
  BaseType_t  xReturn = pdFAIL;

  /* Set frame time stamp */
  if(pullTimeStamp != NULL){
    uint8_t  *ptr = (uint8_t *)pullTimeStamp;
    uint64_t  pullTime;
    xHalTimerGet(&pullTime);
    pullTime = rtnet_htonll(pullTime);
    memcpy(ptr, &pullTime, sizeof(uint64_t));
  }
  if(xNetwork.xMac->SendFrame(pucBuffer, xDataLength, 0) == ARM_DRIVER_OK)
    xReturn = pdPASS;
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t xHalNetworkInterfaceCheckLinkStatus(void)
{
  ARM_ETH_LINK_STATE link;

  link = xNetwork.xPhy->GetLinkState();
  if (link == xNetwork.link) {
    return (BaseType_t) link; // link state unchanged
  }
  // link state changed
  xNetwork.link = link;
  if (link == ARM_ETH_LINK_UP) { // start transfer
    ARM_ETH_LINK_INFO info = xNetwork.xPhy->GetLinkInfo ();
    xNetwork.xMac->Control(ARM_ETH_MAC_CONFIGURE,
                           info.speed  << ARM_ETH_MAC_SPEED_Pos  |
                           info.duplex << ARM_ETH_MAC_DUPLEX_Pos |
                           ARM_ETH_MAC_ADDRESS_BROADCAST);
    xNetwork.xMac->Control(ARM_ETH_MAC_CONTROL_TX, 1);
    xNetwork.xMac->Control(ARM_ETH_MAC_CONTROL_RX, 1);
  }
  else { // stop transfer
    xNetwork.xMac->Control(ARM_ETH_MAC_FLUSH,
                           ARM_ETH_MAC_FLUSH_TX | ARM_ETH_MAC_FLUSH_RX);
    xNetwork.xMac->Control(ARM_ETH_MAC_CONTROL_TX, 0);
    xNetwork.xMac->Control(ARM_ETH_MAC_CONTROL_RX, 0);
  }
  return (BaseType_t) link;
}
/*---------------------------------------------------------------------------*/

extern void vEthernetTimerNotify(BaseType_t  *hptw);

void prvEthernetMacNotify(uint32_t event)
{
  BaseType_t hptw = false;

  /* Handle receive event */
  if(event & ARM_ETH_MAC_EVENT_RX_FRAME){
    /* Get frame time stamp */
    //xHalTimerGet(&xNetwork.ullTimeStamp);
    /* Wake up task */
    vTaskNotifyGiveFromISR(xNetwork.xTaskEthRxEvent, &hptw);
  }
  if(event & ARM_ETH_MAC_EVENT_TIMER_ALARM){
    if(hptw) vEthernetTimerNotify(NULL);
    else vEthernetTimerNotify(&hptw);
  }
  portEND_SWITCHING_ISR(hptw);
}
/*---------------------------------------------------------------------------*/

void prvETHDeferredInterruptHandlerTask(void *pvParameters)
{
  xNetworkDev_t      *pxDev = pvParameters;
  ARM_DRIVER_ETH_MAC *xMac  = pxDev->xMac;
  uint64_t            ullTimeStamp;
  uint32_t            ulEventNumber;

  /* Wait for clock stabilization */
  vTaskDelay(RTNET_MS_TO_TIC(500));
  /* Tune ETH 1588 timer */
  prvTuneEth1588Timer(RTNET_MS_TO_TIC(10),  1000);
  /* Wait for link up */
  while(!xHalNetworkInterfaceCheckLinkStatus()){
    vTaskDelay(RTNET_MS_TO_TIC(200));
  }
  /* Main loop */
	for( ;; ){
	  xRTbuf_t *pxRTbuf;
		/* Wait for the ETH interrupt to indicate that another packet has been
		received. */
	  ulEventNumber = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
	  /* Process all received packets */
	  while(ulEventNumber > 0){
	    /* Allocate a network buffer descriptor that references an Ethernet
	       buffer large enough to hold the received data. */
	    pxRTbuf = pxRTbufGet(xMac->GetRxFrameSize(), (portTickType) 0);
	    if(pxRTbuf != NULL){
	      ARM_ETH_MAC_TIME xTime;
	      /* Get frame timestamp */
	      xMac->GetRxFrameTime(&xTime);
	      ullTimeStamp = TIME_TO_UINT64(xTime.sec, xTime.ns);
	      /* Copy the received data into RTbuf. */
	      xMac->ReadFrame(pxRTbuf->pucData, pxRTbuf->ulSize);
	      /* See if the data contained in the received Ethernet frame needs
	         to be processed. */
	      switch(eRTnetCheckFrame(pxRTbuf->pucData)){
	      case eRTnetFrameDiscipline:
	      case eRTnetFrameIPv4:
	        xRTnetProcessFrame(pxRTbuf, ullTimeStamp);
	        break;
	      case eRTnetFrameOther:
	      case eRTnetFrameNotRecipient:
	        /* Release the Ethernet buffer */
	        vRTbufRelease(pxRTbuf);
	        break;
	      }
	    }
	    else{
	      /* Drop received frame */
	      xMac->ReadFrame(NULL, 0);
	    }
	    /* Event has been processed */
	    ulEventNumber--;
	  }
	}
}
/*---------------------------------------------------------------------------*/

void prvTuneEth1588Timer(TickType_t xPeriod, uint32_t ulRepeat)
{
  ARM_ETH_MAC_TIME xTime;
  TickType_t       xLastWakeTime;
  uint64_t         ullSlaveClockTime, ullSlaveClockTimePrv;
  uint64_t         ullMasterClockCount;
  uint32_t         ulIdx;

  /* Calculate master clock count for all cycles */
  ullMasterClockCount  = xPeriod;
  ullMasterClockCount *= 1000000000UL;
  ullMasterClockCount /= configTICK_RATE_HZ;
  /* Get current slave time */
  xLastWakeTime = xTaskGetTickCount();
  xNetwork.xMac->ControlTimer(ARM_ETH_MAC_TIMER_GET_TIME, &xTime);
  ullSlaveClockTimePrv = TIME_TO_UINT64(xTime.sec, xTime.ns);
  /* Loop for sync */
  for(ulIdx = 0; ulIdx < ulRepeat; ulIdx++){
    vTaskDelayUntil(&xLastWakeTime, xPeriod);
    xNetwork.xMac->ControlTimer(ARM_ETH_MAC_TIMER_GET_TIME, &xTime);
    ullSlaveClockTime    = TIME_TO_UINT64(xTime.sec, xTime.ns);
    vEth1588TimerTune(ullMasterClockCount,
                      ullSlaveClockTime - ullSlaveClockTimePrv);
    ullSlaveClockTimePrv = ullSlaveClockTime;
    XMC_GPIO_ToggleOutput(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_ETH_PIN);
  }
  XMC_GPIO_SetOutputHigh(BSPRTNODEBASE_LED_DEV, BSPRTNODEBASE_LED_ETH_PIN);
}
/*---------------------------------------------------------------------------*/
