/*****************************************************************************
 *                                                                           *
 *   rtbuf.c                                                                 *
 *                                                                           *
 *   Real-time data buffers                                                  *
 *                                                                           *
 *   This file should not be used if the project includes a memory allocator *
 *   that will fragment the heap memory.  For example, heap_2 must not be    *
 *   used, heap_4 can be used.                                               *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <stdint.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

/* RTnet includes */
#include "RTnetConfig.h"
#include "rtbuf.h"
#include "rtnet_mem.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

struct xRTBUF_DEV{
  xRTbuf_t          xRTbuf[rtnetconfigRTBUF_NUMBER];
  xList             xFree;
  uint32_t          ulReady;
  //SemaphoreHandle_t xSem;
  //SemaphoreHandle_t xMut;
};
typedef struct xRTBUF_DEV xRTbufDev_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

void prvRTbufClear(xRTbuf_t *pxBuf);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

static xRTbufDev_t gxRTbufDev;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTbufInit(void)
{
  BaseType_t  x;

  /* Create TX semaphore */
//  gxRTbufDev.xSem = xSemaphoreCreateCounting(rtnetconfigRTBUF_NUMBER,
//                                             rtnetconfigRTBUF_NUMBER);
//  if(gxRTbufDev.xSem == NULL) return pdFAIL;
//  gxRTbufDev.xMut = xSemaphoreCreateMutex();
//  if(gxRTbufDev.xMut == NULL){
//    vSemaphoreDelete(gxRTbufDev.xSem);
//    return pdFAIL;
//  }
  gxRTbufDev.ulReady = rtnetconfigRTBUF_NUMBER;
  vListInitialise(&gxRTbufDev.xFree);
  for(x = 0; x < rtnetconfigRTBUF_NUMBER; x++){
    xRTbuf_t *pxRTbuf;
    pxRTbuf = gxRTbufDev.xRTbuf + x;
    prvRTbufClear(pxRTbuf);
    vListInitialiseItem(&pxRTbuf->xItem);
    listSET_LIST_ITEM_OWNER(&pxRTbuf->xItem, pxRTbuf);
    vListInsert(&gxRTbufDev.xFree, &pxRTbuf->xItem);
  }
  return pdPASS;
}

xRTbuf_t *pxRTbufGet(size_t xSize, portTickType xTicksToWait)
{
  xRTbuf_t *pxRTbuf = NULL;

  //if(xSemaphoreTake(gxRTbufDev.xSem, xTicksToWait) == pdPASS){
    //xSemaphoreTake(gxRTbufDev.xMut, portMAX_DELAY);
    taskENTER_CRITICAL();
    {
      if(gxRTbufDev.ulReady == 0){
        taskEXIT_CRITICAL();
        return NULL;
      }
      gxRTbufDev.ulReady--;

      pxRTbuf = listGET_OWNER_OF_HEAD_ENTRY(&gxRTbufDev.xFree);
      uxListRemove(&pxRTbuf->xItem);
    }
    //xSemaphoreGive(gxRTbufDev.xMut);
    taskEXIT_CRITICAL();
    if(xSize > 0){
      pxRTbuf->pucData = rtnet_malloc(xSize);
      if(pxRTbuf->pucData == NULL){
        vRTbufRelease(pxRTbuf);
        return NULL;
      }
      pxRTbuf->ulSize = (uint32_t) xSize;
    }
  //}
  return pxRTbuf;
}

void vRTbufRelease(xRTbuf_t *pxBuf)
{
  if(pxBuf == NULL) return;
  rtnet_free(pxBuf->pucData);
  prvRTbufClear(pxBuf);
  //xSemaphoreTake(gxRTbufDev.xMut, portMAX_DELAY);
  taskENTER_CRITICAL();
  {
    if(listIS_CONTAINED_WITHIN(&gxRTbufDev.xFree, &(pxBuf->xItem)) == pdFALSE)
      vListInsertEnd(&gxRTbufDev.xFree, &(pxBuf->xItem));
    gxRTbufDev.ulReady++;
  }
  //xSemaphoreGive(gxRTbufDev.xMut);
  taskEXIT_CRITICAL();
  //xSemaphoreGive(gxRTbufDev.xSem);
}

void prvRTbufClear(xRTbuf_t *pxBuf)
{
  pxBuf->pucData = NULL;
  pxBuf->ulSize  = 0;
  pxBuf->ulIdx   = 0;
}
