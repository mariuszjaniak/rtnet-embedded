/*****************************************************************************
 *                                                                           *
 *   rtarp.h                                                                 *
 *                                                                           *
 *   RT ARP -- real-time Address Resolution Protocol                         *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

/* RTnet includes */
#include "rtarp.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static xRTarpEnt_t *prvRTarpFindByMac(xRTarpDev_t *pxDev, uint8_t *pucMac);
static xRTarpEnt_t *prvRTarpFindByIp(xRTarpDev_t *pxDev, uint32_t ulIpAddr);
static void         prvRTarpDelEnt(xRTarpDev_t *pxDev, xRTarpEnt_t *pxEnt);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTarpInit(xRTarpDev_t *pxDev)
{
#ifdef DEBUG
  if(pxDev == NULL) return pdFAIL;
#endif
  memset(pxDev->pucTab, 0, sizeof(pxDev->pucTab));
  pxDev->ulLen = 0;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpAdd(xRTarpDev_t *pxDev,
                        uint8_t     *pucMac,
                        uint32_t     ulIpAddr)
{
  uint32_t ulIdx;

#ifdef DEBUG
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
#endif
  ulIdx = pxDev->ulLen;
  if(ulIdx >= rtnetconfigARP_TAB_SIZE) return pdFAIL;
  if(prvRTarpFindByMac(pxDev, pucMac) != NULL) return pdFAIL;
  if(prvRTarpFindByIp(pxDev, ulIpAddr) != NULL) return pdFAIL;
  {
    xRTarpEnt_t *pxEnt;
    pxEnt = pxDev->pucTab + ulIdx;
    memcpy(pxEnt->pucMac, pucMac, rtarpETHERNET_MAC_SIZE);
    pxEnt->ulIpAddr = ulIpAddr;
    pxEnt->eState = eRTarpStateInit;
  }
  pxDev->ulLen++;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpDelIp(xRTarpDev_t *pxDev,
                          uint32_t     ulIpAddr)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if(pxDev == NULL) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByIp(pxDev, ulIpAddr);
  if(pxEnt == NULL) return pdFAIL;
  prvRTarpDelEnt(pxDev, pxEnt);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpDelMac(xRTarpDev_t *pxDev,
                           uint8_t     *pucMac)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByMac(pxDev, pucMac);
  if(pxEnt == NULL) return pdFAIL;
  prvRTarpDelEnt(pxDev, pxEnt);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpHasMac(xRTarpDev_t *pxDev,
                           uint8_t     *pucMac)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByMac(pxDev, pucMac);
  if(pxEnt == NULL) return pdFAIL;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpGetIp(xRTarpDev_t *pxDev,
                          uint8_t     *pucMac,
                          uint32_t    *pulIpAddr)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByMac(pxDev, pucMac);
  if(pxEnt == NULL) return pdFAIL;
  if(pxEnt->ulIpAddr == rtarpWITHOUT_IP) return pdFAIL;
  if(pulIpAddr != NULL) *pulIpAddr = pxEnt->ulIpAddr;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpGetMac(xRTarpDev_t *pxDev,
                           uint8_t     *pucMac,
                           uint32_t     ulIpAddr)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if(pxDev == NULL) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByIp(pxDev, ulIpAddr);
  if(pxEnt == NULL) return pdFAIL;
  if(pucMac != NULL) memcpy(pucMac, pxEnt->pucMac, rtarpETHERNET_MAC_SIZE);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

uint8_t *pucRTarpGetMacPtr(xRTarpDev_t *pxDev,
                           uint32_t     ulIpAddr)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if(pxDev == NULL) return NULL;
#endif
  pxEnt = prvRTarpFindByIp(pxDev, ulIpAddr);
  if(pxEnt == NULL) return NULL;
  return pxEnt->pucMac;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpSetState(xRTarpDev_t   *pxDev,
                             uint8_t       *pucMac,
                             eRTarpState_t  eState)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByMac(pxDev, pucMac);
  if(pxEnt == NULL) return pdFAIL;
  pxEnt->eState = eState;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTarpGetState(xRTarpDev_t   *pxDev,
                             uint8_t       *pucMac,
                             eRTarpState_t *eState)
{
  xRTarpEnt_t *pxEnt;

#ifdef DEBUG
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
#endif
  pxEnt = prvRTarpFindByMac(pxDev, pucMac);
  if(pxEnt == NULL) return pdFAIL;
  if(eState != NULL) *eState = pxEnt->eState;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

xRTarpEnt_t *prvRTarpFindByMac(xRTarpDev_t *pxDev, uint8_t *pucMac)
{
  uint32_t i;

  for(i = 0; i < pxDev->ulLen; ++i){
    xRTarpEnt_t *pxEnt;
    pxEnt = pxDev->pucTab + i;
    if(memcmp(pucMac, pxEnt->pucMac, rtarpETHERNET_MAC_SIZE) == 0)
      return pxEnt;
  }
  return NULL;
}
/*---------------------------------------------------------------------------*/

xRTarpEnt_t *prvRTarpFindByIp(xRTarpDev_t *pxDev, uint32_t ulIpAddr)
{
  uint32_t i;

  if(ulIpAddr == rtarpWITHOUT_IP) return NULL;
  for(i = 0; i < pxDev->ulLen; ++i){
    xRTarpEnt_t *pxEnt;
    pxEnt = pxDev->pucTab + i;
    if(ulIpAddr == pxEnt->ulIpAddr) return pxEnt;
  }
  return NULL;
}
/*---------------------------------------------------------------------------*/

void prvRTarpDelEnt(xRTarpDev_t *pxDev, xRTarpEnt_t *pxEnt)
{
  uint32_t ulIdx;

  ulIdx = pxDev->pucTab - pxEnt;
  /* Check if is last entry. */
  if(ulIdx == pxDev->ulLen - 1){
    pxDev->ulLen--;
    memset(pxEnt, 0, sizeof(xRTarpEnt_t));
  }
  else{
    xRTarpEnt_t *pxLast;
    pxLast = pxDev->pucTab + pxDev->ulLen - 1;
    pxDev->ulLen--;
    memcpy(pxEnt, pxLast, sizeof(xRTarpEnt_t));
    memset(pxLast, 0, sizeof(xRTarpEnt_t));
  }
}
/*---------------------------------------------------------------------------*/

