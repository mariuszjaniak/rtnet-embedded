/*****************************************************************************
 *                                                                           *
 *   rtnet_common.c                                                          *
 *                                                                           *
 *   RTnet common data types and utilities                                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <stdarg.h>
#include <stdint.h>

/* RTnet includes */
#include "rtnet_common.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

uint16_t rtnet_checksum(uint8_t *pucData, uint16_t  usLen)
{
  int32_t   lSum    = 0;
  uint16_t  usCount = usLen;
  uint16_t *pusAddr = (uint16_t *) pucData;

  while(usCount > 1){
    lSum += *pusAddr++;
    usCount -= 2;
  }
  /*  Add left-over byte, if any */
  if(usCount > 0) lSum += *((uint8_t *) pusAddr);
  /*  Fold 32-bit sum to 16 bits */
  while (lSum >> 16) lSum = (lSum & 0xFFFF) + (lSum >> 16);
  return ~((uint16_t) lSum);
}
/*---------------------------------------------------------------------------*/
