/*****************************************************************************
 *                                                                           *
 *   rtnetDev.c                                                              *
 *                                                                           *
 *   RTnet device                                                            *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

/* RTnet includes */
#include "rtnetDev.h"
#include "halNetworkInterface.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtnetdevMAGIC_NUMBER       (0x5A5A5A5A)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/*****************************************************************************
 * Globals variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTnetDevInit(xRTnetDev_t *pxDev, uint8_t pucMac[])
{
  if((pxDev == NULL) || (pucMac == NULL)) return pdFAIL;
  /* Initialize locals */
  memcpy(pxDev->pucMac, pucMac, rtnetdevMAC_SIZE);
  pxDev->ulMagic   = rtnetdevMAGIC_NUMBER;
  pxDev->ulRxFrame = 0;
  pxDev->ulTxFrame = 0;
  /* Initialize RTmac */
  if(xRTmacInit(&pxDev->xRTmac) == pdFAIL) return pdFAIL;
  /* Initialize network interface */
  if(xHalNetworkInterfaceInitialise(pxDev->pucMac) == pdFAIL)
    return pdFAIL;
  if(xRTcfgInit(&pxDev->xRTcfg) == pdFAIL) return pdFAIL;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTnetDevIsInit(xRTnetDev_t *pxDev)
{
  if(pxDev->ulMagic != rtnetdevMAGIC_NUMBER) return pdFALSE;
  return pdTRUE;
}
/*---------------------------------------------------------------------------*/
