/*****************************************************************************
 *                                                                           *
 *   rmac.c                                                                  *
 *                                                                           *
 *   RTmac -- real-time media access control framework for RTnet             *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <stdarg.h>
#include <stdint.h>

/* RTnet includes */
#include "rtnet_inet.h"
#include "rtnet_struct.h"
#include "rtmac.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtmacFRAME_VERSION          0x02

#define rtmacFLAGS_DISCIPLINE       0x00
#define rtmacFLAGS_TUNNELLING_MASK  0x01

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

RTNET_STRUCT_BEGIN();
struct xRTMAC_HEADER{
  uint16_t usType;
  uint8_t  ucVersion;
  uint8_t  ucFlags;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTMAC_HEADER xRTmacHeader_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static BaseType_t  prvRxTunnelling(xRTmacDev_t *pxDev,
                                     uint16_t     usType,
                                     uint8_t     *frame);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTmacInit(xRTmacDev_t  *pxDev)
{
  if(pxDev == NULL) return pdFAIL;
  /* Init TDMA discipline */
  return xTdmaInit(&pxDev->xTdma);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTmacProcessFrame(xRTmacDev_t *pxDev,
                                 uint8_t      pucFrame[],
                                 uint64_t     ullTimeStamp)
{
  xRTmacHeader_t *pxHeader = (xRTmacHeader_t*) pucFrame;
  BaseType_t    xReturn  = pdPASS;

  if(pxHeader->ucVersion != rtmacFRAME_VERSION) return pdFALSE;
  if(pxHeader->ucFlags & rtmacFLAGS_TUNNELLING_MASK){
    xReturn = prvRxTunnelling(pxDev,
                              pxHeader->usType,
                              pucFrame + sizeof(xRTmacHeader_t));
  }
  else{
    switch(rtnet_ntohs(pxHeader->usType)){
    case tdmaRTMAC_TYPE:
      xReturn = xTdmaProcessFrame(&pxDev->xTdma,
                                  pucFrame + sizeof(xRTmacHeader_t),
                                  ullTimeStamp);
      break;
    default:
      return pdFAIL;
    }
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTmacSend(xRTmacDev_t *pxDev,
                       uint8_t      pucBuffer[],
                       uint32_t     ulLen)
{
  return xTdmaSend(&pxDev->xTdma, pucBuffer, ulLen);
}
/*---------------------------------------------------------------------------*/

size_t xRTmacHeaderSize(void)
{
  return sizeof(xRTmacHeader_t);
}
/*---------------------------------------------------------------------------*/

uint32_t ulRTmacHeader(uint8_t pucFrame[], eRTmacFrame_t eFrameType, ...)
{
  xRTmacHeader_t *pxHeader = (xRTmacHeader_t*) pucFrame;
  va_list         xArg;

  /* Start the varargs processing */
  va_start(xArg, eFrameType);
  /* Prepare header */
  switch(eFrameType){
  case eRTmacFrameTdma:
    pxHeader->usType    = rtnet_htons(tdmaRTMAC_TYPE);
    pxHeader->ucVersion = rtmacFRAME_VERSION;
    pxHeader->ucFlags   = rtmacFLAGS_DISCIPLINE;
    break;
  case eRTmacFrameTunel:
    {
      int      tmp;
      uint16_t usType;
      tmp               = va_arg(xArg, int);
      usType            = (uint16_t) tmp;
      pxHeader->usType  = rtnet_htons(usType);
    }
    pxHeader->ucVersion = rtmacFRAME_VERSION;
    pxHeader->ucFlags   = rtmacFLAGS_TUNNELLING_MASK;
    break;
  }
  /* End the varargs processing */
  va_end(xArg);
  return sizeof(xRTmacHeader_t);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTmacUpdateLinkStatus(void)
{
  return xTdmaUpdateLinkStatus();
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTmacIsReady(xRTmacDev_t *pxDev)
{
  return xTdmaIsReady(&pxDev->xTdma);
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxTunnelling(xRTmacDev_t *pxDev,
                              uint16_t     usType,
                              uint8_t     *frame)
{
  /* TODO: Implement tunneling frames */
  return pdPASS;
}
/*---------------------------------------------------------------------------*/
