/*****************************************************************************
 *                                                                           *
 *   rtsocket.h                                                              *
 *                                                                           *
 *   Real-time sockets                                                       *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <stdint.h>
#include <string.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "task.h"

/* RTnet includes */
#include "RTnetConfig.h"
#include "rtnet_mem.h"
#include "rtnet_struct.h"
#include "rtnet_private.h"
#include "rtipv4.h"
#include "rtudp.h"
#include "rtsocket.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define rtsocketSET_ADDR(pxSocket, usPort) listSET_LIST_ITEM_VALUE(&(pxSocket)->xItem, usPort)
#define rtsocketGET_ADDR(pxSocket)         listGET_LIST_ITEM_VALUE(&(pxSocket)->xItem)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

RTNET_STRUCT_BEGIN();
struct xRTSOCKETIPV4_DSC{
  xRTnetSockAddr_t  xSrcAddr;
  uint16_t          usLen;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTSOCKETIPV4_DSC xRTsocketIpv4Dsc_t;

struct xRTSOCKET_DEV
{
  xList xBoundUdpSockets;
};
typedef struct xRTSOCKET_DEV xRTsocketDev_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static xListItem *prvFindItem(xList *pxList, portTickType xValue);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

xRTsocketDev_t gxDev;

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

void vRTsocketInit(void)
{
  vListInitialise(&gxDev.xBoundUdpSockets);
}
/*---------------------------------------------------------------------------*/

xRTsocket_t *pxRTsocketOpen(uint32_t ulDomain,
                            uint32_t ulType,
                            uint32_t ulProtocol)
{
  xRTsocket_t *pxSocket = NULL;

  if(!listLIST_IS_INITIALISED(&gxDev.xBoundUdpSockets)) return NULL;
  pxSocket = rtnet_malloc(sizeof(xRTsocket_t));
  if(pxSocket == NULL) return NULL;
  switch(ulDomain){
  case RTNET_AF_INET:
    switch(ulType){
    case RTNET_SOCK_DGRAM:
      switch(ulProtocol){
      case 0:
      case RTNET_IPPROTO_UDP:
        pxSocket->xRxSem = NULL;
        vListInitialise(&pxSocket->xPacketList);
        vListInitialiseItem(&pxSocket->xItem);
        listSET_LIST_ITEM_OWNER(&pxSocket->xItem, pxSocket);
        pxSocket->xTxBlockTime = ( portTickType ) 0;
        pxSocket->xRxBlockTime = portMAX_DELAY;
        pxSocket->ucOpt        = RTNET_SO_UDPCKSUM;
        break;
      default:
        rtnet_free(pxSocket);
        return NULL;
      }
      break;
    default:
      rtnet_free(pxSocket);
      return NULL;
    }
    break;
  default:
    rtnet_free(pxSocket);
    return NULL;
  }
  pxSocket->ulDomain   = ulDomain;
  pxSocket->ulType     = ulType;
  pxSocket->ulProtocol = ulProtocol;
  return pxSocket;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTsecketClose(xRTsocket_t *pxSocket)
{
  if(pxSocket == NULL) return RTNET_SOCKET_ERROR;
  /* Unbound socket */
  if(pxSocket->xRxSem != NULL){
    taskENTER_CRITICAL();
    {
      uxListRemove(&pxSocket->xItem);
    }
    taskEXIT_CRITICAL();
    while(listCURRENT_LIST_LENGTH(&pxSocket->xPacketList) > 0U)
    {
      xRTbuf_t *pxBuf;
      pxBuf = listGET_OWNER_OF_HEAD_ENTRY(&pxSocket->xPacketList);
      uxListRemove(&pxBuf->xItem);
      vRTbufRelease(pxBuf);
    }
    vSemaphoreDelete(pxSocket->xRxSem);
  }
  rtnet_free(pxSocket);
  return 0;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTsocketBind(xRTsocket_t            *pxSocket,
                            const xRTnetSockAddr_t *pxAddr,
                            uint32_t                ulAddrlen)
{
  (void) ulAddrlen;
  if(pxSocket == NULL) return RTNET_SOCKET_ERROR;
  if(pxAddr == NULL) return RTNET_EADDRNOTAVAIL;
  if(pxAddr->sin_port == 0) return RTNET_EADDRNOTAVAIL;
  vTaskSuspendAll();
  {
    if(prvFindItem(&gxDev.xBoundUdpSockets,
                   (portTickType) pxAddr->sin_port) != NULL)
      return RTNET_EADDRINUSE;
  }
  xTaskResumeAll();
  if(pxSocket->xRxSem != NULL) return RTNET_EINVAL;
  pxSocket->xRxSem = xSemaphoreCreateCounting(rtnetconfigRTBUF_NUMBER, 0);
  if(pxSocket->xRxSem == NULL) return RTNET_ENOMEM;
  /* Allocate the port number to the socket. */
  rtsocketSET_ADDR(pxSocket, pxAddr->sin_port);
  taskENTER_CRITICAL();
  {
    vListInsertEnd(&gxDev.xBoundUdpSockets, &pxSocket->xItem);
  }
  taskEXIT_CRITICAL();
  return 0;
}
/*---------------------------------------------------------------------------*/

int32_t lRTsocketRecvfrom(xRTsocket_t      *pxSocket,
                          void             *pvBuf,
                          size_t            xLen,
                          uint32_t          ulFlags,
                          xRTnetSockAddr_t *pxSrcAddr,
                          uint32_t         *pulAddrlen)
{
  xRTbuf_t *pxBuf;

  (void) pulAddrlen;
  if(pxSocket == NULL) return RTNET_SOCKET_ERROR;
  if(pxSocket->xRxSem == NULL) return RTNET_EINVAL;
  if(xSemaphoreTake(pxSocket->xRxSem, pxSocket->xRxBlockTime) != pdPASS){
    if(pxSocket->xRxBlockTime == 0) return RTNET_EWOULDBLOCK;
    else return RTNET_TIMEOUT;
  }
  taskENTER_CRITICAL();
  {
    pxBuf = listGET_OWNER_OF_HEAD_ENTRY(&pxSocket->xPacketList);
    uxListRemove(&pxBuf->xItem);
  }
  taskEXIT_CRITICAL();
  switch(pxSocket->ulDomain){
  case RTNET_AF_INET:
    switch(pxSocket->ulType){
    case RTNET_SOCK_DGRAM:
      switch(pxSocket->ulProtocol){
      case 0:
      case RTNET_IPPROTO_UDP:
        {
          uint8_t            *pucData;
          xRTsocketIpv4Dsc_t *pxDsc;
          pucData = pxBuf->pucData + pxBuf->ulIdx;
          pxDsc   = (xRTsocketIpv4Dsc_t*)(pucData - sizeof(xRTsocketIpv4Dsc_t));
          if(ulFlags & RTNET_ZERO_COPY){
            *((void **) pvBuf) = pucData;
            xLen   = pxDsc->usLen;
            memcpy(pucData - sizeof(uint8_t*),
                   &pxBuf->pucData,
                   sizeof(uint8_t*));
            pxBuf->pucData = NULL;
          }
          else{
            if(pxDsc->usLen < xLen) xLen = pxDsc->usLen;
            memcpy(pvBuf, pucData, xLen);
          }
          vRTbufRelease(pxBuf);
          if(pxSrcAddr != NULL)
            memcpy(pxSrcAddr, &pxDsc->xSrcAddr, sizeof(xRTnetSockAddr_t));
        }
        break;
      default:
        vRTbufRelease(pxBuf);
        return RTNET_SOCKET_ERROR;
      }
      break;
    default:
      vRTbufRelease(pxBuf);
      return RTNET_SOCKET_ERROR;
    }
    break;
  default:
    vRTbufRelease(pxBuf);
    return RTNET_SOCKET_ERROR;
  }
  return xLen;
}
/*---------------------------------------------------------------------------*/

int32_t lRTsocketSendto(xRTsocket_t      *pxSocket,
                        void             *pvBuf,
                        size_t            xLen,
                        uint32_t          ulFlags,
                        xRTnetSockAddr_t *pxDestAddr,
                        uint32_t          ulAddrlen)
{
  uint8_t *pucFrame;

  (void) ulAddrlen;
  if(pxSocket == NULL) return RTNET_SOCKET_ERROR;
  if(pxDestAddr == NULL) return RTNET_EADDRNOTAVAIL;
  if(pxSocket->xRxSem == NULL) return RTNET_EINVAL;
  switch(pxSocket->ulDomain){
  case RTNET_AF_INET:
    switch(pxSocket->ulType){
    case RTNET_SOCK_DGRAM:
      switch(pxSocket->ulProtocol){
      case 0:
      case RTNET_IPPROTO_UDP:
        {
          xRTnetSockAddr_t  xSrcAddr;
          size_t            xTotSize;
          uint32_t          ulSize;
          uint8_t          *pucMac;
          xTotSize = xRTipv4UdpSize(xLen);
          if(xTotSize > rtnetETHERNET_MTU_SIZE) return RTNET_EMSGSIZE;
          pucMac = pucRTnetGetMacPtr(pxDestAddr->sin_addr);
          if(pucMac == NULL) return RTNET_EADDRNOTAVAIL;
          xSrcAddr.sin_addr = ulRTnetIpAddr();
          xSrcAddr.sin_port = rtsocketGET_ADDR(pxSocket);
          /* Prepare frame */
          if(ulFlags & RTNET_ZERO_COPY){
            memcpy(&pucFrame,
                   (uint8_t*) pvBuf - sizeof(uint8_t*),
                   sizeof(uint8_t*));
            ulSize  = ulRTnetHeader(pucFrame, pucMac, eRTnetFrameEthernet);
          }
          else{
            pucFrame = pucRTnetPrepareFrame(&ulSize,
                                           xTotSize,
                                           pucMac,
                                           eRTnetFrameEthernet);
            if(pucFrame == NULL) return RTNET_ENOMEM;
          }
          ulSize +=
              ulRTipv4DefUdpFrame(pucFrame + ulSize,
                                  &xSrcAddr,
                                  pxDestAddr,
                                  pvBuf,
                                  xLen,
                                  pxSocket->ucOpt);
          if(xRTnetSendFrame(pucFrame,
                             ulSize,
                             pxSocket->xTxBlockTime) == pdFAIL){
            if(!(ulFlags & RTNET_ZERO_COPY)) rtnet_free(pucFrame);
            if(pxSocket->xTxBlockTime == 0) return RTNET_EWOULDBLOCK;
            else return RTNET_TIMEOUT;
          }
        }
        break;
      default:
        return RTNET_SOCKET_ERROR;
      }
      break;
    default:
      return RTNET_SOCKET_ERROR;
    }
    break;
  default:
    return RTNET_SOCKET_ERROR;
  }
  return (uint32_t) xLen;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTsocketSetsockopt(xRTsocket_t *pxSocket,
                                 int32_t      lLevel,
                                 int32_t      lOptname,
                                 const void  *pvOptval,
                                 size_t       xOptlen)
{
  (void) lLevel;
  (void) xOptlen;
  switch(lOptname){
  case RTNET_SO_RXTIMEOUT:
    pxSocket->xRxBlockTime = *(( portTickType * ) pvOptval);
    break;
  case RTNET_SO_TXTIMEOUT:
    pxSocket->xTxBlockTime = *(( portTickType * ) pvOptval);
    break;
  case RTNET_SO_UDPCKSUM:
    if(((BaseType_t ) pvOptval) == 0)
      pxSocket->ucOpt &= ~RTNET_SO_UDPCKSUM;
    else
      pxSocket->ucOpt|= RTNET_SO_UDPCKSUM;
    break;
  default:
    return RTNET_ENOPROTOOPT;
  }
  return 0;
}
/*---------------------------------------------------------------------------*/

void *pvRTsocketGetUdpDataBuffer(size_t xSize)
{
  size_t  xLen;
  uint8_t *pucBuf;

  if(xSize > rtnetETHERNET_MTU_SIZE) return NULL;
  xLen = xRTnetHeaderSize() + xRTipv4HeaderSize() + xRTudpHeaderSize();
  pucBuf = rtnet_malloc(xLen + xSize);
  if(pucBuf == NULL) return NULL;
  memcpy(pucBuf + xLen - sizeof(uint8_t*), &pucBuf, sizeof(uint8_t*));
  return pucBuf + xLen;
}
/*---------------------------------------------------------------------------*/

void vRTsocketReleaseUdpDataBuffer(void *pvData)
{
  uint8_t *pucBuf;

  if(pvData == NULL) return;
  memcpy(&pucBuf, (uint8_t*) pvData - sizeof(uint8_t*), sizeof(uint8_t*));
  rtnet_free(pucBuf);
}
/*---------------------------------------------------------------------------*/

BaseType_t  xRTsocketProcessUDPFrame(xRTbuf_t         *pxBuf,
                                       uint32_t          usLen,
                                       uint16_t          usPort,
                                       xRTnetSockAddr_t *pxSrcAddr)
{
  xListItem   *pxItem;
  xRTsocket_t *pxSocket;

  vTaskSuspendAll();
  {
    pxItem = prvFindItem(&gxDev.xBoundUdpSockets, (portTickType) usPort);
  }
  xTaskResumeAll();
  if(pxItem == NULL) return pdFAIL;
  /* Copy UDP descriptor before data block */
  {
    xRTsocketIpv4Dsc_t xDsc;
    memcpy(&xDsc.xSrcAddr, pxSrcAddr, sizeof(xRTnetSockAddr_t));
    xDsc.usLen = usLen;
    memcpy(pxBuf->pucData + pxBuf->ulIdx - sizeof(xRTsocketIpv4Dsc_t),
           &xDsc,
           sizeof(xRTsocketIpv4Dsc_t));
  }
  pxSocket = listGET_LIST_ITEM_OWNER(pxItem);
  taskENTER_CRITICAL();
  {
    vListInsertEnd(&pxSocket->xPacketList, &pxBuf->xItem);
  }
  taskEXIT_CRITICAL();
  xSemaphoreGive(pxSocket->xRxSem);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

xListItem *prvFindItem(xList *pxList, portTickType xValue)
{
  xListItem *pxItem;

  for(pxItem  = (xListItem*) pxList->xListEnd.pxNext;
      pxItem != (xListItem*) &(pxList->xListEnd);
      pxItem  = (xListItem*) pxItem->pxNext )
    if(pxItem->xItemValue == xValue) return pxItem;
  return NULL;
}
/*---------------------------------------------------------------------------*/
