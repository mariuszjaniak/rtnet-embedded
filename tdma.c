/*****************************************************************************
 *                                                                           *
 *   tdma.c                                                                  *
 *                                                                           *
 *   TDMA (Time Division Multiple Access) RTmac discipline                   *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

/* RTnet includes */
#include "rtnet_inet.h"
#include "rtnet_mem.h"
#include "rtnet_struct.h"
#include "rtnet_private.h"
#include "halTimerInterface.h"
#include "halNetworkInterface.h"
#include "tdma.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define tdmaFRAME_VERSION                0x0201
#define tdmaFRAME_ID_SYNCHRONISATION     0x0000
#define tdmaFRAME_ID_CALIBRATION_REQUEST 0x0010
#define tdmaFRAME_ID_CALIBRATION_REPLY   0x0011

#define tdmaINIT_PHASE_CYCLE_NUMBER      50
#define tdmaCALIB_PHASE_CYCLE_NUMBER     50

#define tdmaCALIB_REQUEST_CYCLE_OFFSET   1

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

RTNET_STRUCT_BEGIN();
struct xTDMA_HEADER{
  uint16_t usVersion;
  uint16_t usFrameID;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xTDMA_HEADER xTdmaHeader_t;

RTNET_STRUCT_BEGIN();
struct xTDMA_SYNCHRONISATION{
  xTdmaHeader_t xHeader;
  uint32_t      ulCycleNumber;
  uint64_t      ullTransmissionTimeStamp;
  uint64_t      ullScheduledTransmissionTime;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xTDMA_SYNCHRONISATION xTdmaSynchronisation_t;

RTNET_STRUCT_BEGIN();
struct xTDMA_CALIBRATION_REQUEST{
  xTdmaHeader_t xHeader;
  uint64_t      ullTransmissionTimeStamp;
  uint32_t      ulReplyCycle;
  uint64_t      ullReplySlotOffset;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xTDMA_CALIBRATION_REQUEST xTdmaCalibrationRequest_t;

RTNET_STRUCT_BEGIN();
struct xTDMA_CALIBRATION_REPLY{
  xTdmaHeader_t xHeader;
  uint64_t      ullRequestTransmissionTime;
  uint64_t      ullReceptionTimeStamp;
  uint64_t      ullTransmissionTimeStamp;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xTDMA_CALIBRATION_REPLY xTdmaCalibrationReply_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static BaseType_t  prvRxSynchronisation(xTdmaDev_t             *pxDev,
                                          xTdmaSynchronisation_t *pxFrame,
                                          uint64_t                ullRecv);

static BaseType_t  prvTdmaSendCalibrationRequest(xTdmaDev_t *pxDev);

static BaseType_t  prvRxCalibrationRequest(xTdmaDev_t                *pxDev,
                                             xTdmaCalibrationRequest_t *pxFrame,
                                             uint64_t                   ullRecv);

static BaseType_t  prvRxCalibrationReply(xTdmaDev_t              *pxDev,
                                           xTdmaCalibrationReply_t *pxFrame,
                                           uint64_t                 ullRecvRpl);

static BaseType_t  prvTimerUpdateRelative(xTdmaDev_t *pxDev,
                                            uint64_t    ullXmit,
                                            uint64_t    ullRecv);

static BaseType_t  prvSlotEvent(xTdmaDev_t *pxDev);

static xTdmaSlot_t *prvFindFirstSlot(xTdmaSlot_t  pxSlots[],
                                     uint32_t     ulLen,
                                     uint32_t     ulCycleStart,
                                     uint32_t    *ulCycleRet,
                                     uint32_t     ulIdxStart,
                                     uint32_t    *ulIdxRet);

static xTdmaSlot_t *prvFindFirstSlotInCycle(xTdmaSlot_t  pxSlots[],
                                            uint32_t     ulLen,
                                            uint32_t     ulCycle,
                                            uint32_t     ulIdxStart,
                                            uint32_t    *ulIdxRet);

static BaseType_t  prvFifoPeekLen(xTdmaFifo_t *pxFifo, size_t *pxLen);
static BaseType_t  prvFifoGet(xTdmaFifo_t *pxFifo, xTdmaDesc_t *pxDesc);
static BaseType_t  prvFifoPut(xTdmaFifo_t *pxFifo, xTdmaDesc_t *pxDesc);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xTdmaInit(xTdmaDev_t *pxDev)
{
  if(pxDev == NULL) return pdFAIL;
  pxDev->eState = eTdmaStatePreInit;
  memset(&pxDev->xTxFifo, 0, sizeof(xTdmaFifo_t));
  memset(&pxDev->xT,      0, sizeof(xTdmaTime_t));
  memset(&pxDev->xCount,  0, sizeof(xTdmaCount_t));
  if(xTdmaCfgInit(&pxDev->xCfg) == pdFAIL) return pdFAIL;
  if(xHalTimerInterfaceInitialise(pxDev) == pdFAIL) return pdFAIL;
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaProcessFrame(xTdmaDev_t *pxDev,
                                uint8_t     pucFrame[],
                                uint64_t    ullTimeStamp)
{
  xTdmaHeader_t *pxHeader = (xTdmaHeader_t*) pucFrame;
  BaseType_t   xReturn  = pdPASS;

  /* Process frame header */
  if(rtnet_ntohs(pxHeader->usVersion) != tdmaFRAME_VERSION) return pdFALSE;
  switch(rtnet_ntohs(pxHeader->usFrameID)){
  case tdmaFRAME_ID_SYNCHRONISATION:
    xReturn = prvRxSynchronisation(pxDev,
                                   (xTdmaSynchronisation_t *) pucFrame,
                                   ullTimeStamp);
    break;
  case tdmaFRAME_ID_CALIBRATION_REQUEST:
    xReturn = prvRxCalibrationRequest(pxDev,
                                      (xTdmaCalibrationRequest_t *) pucFrame,
                                      ullTimeStamp);
    break;
  case tdmaFRAME_ID_CALIBRATION_REPLY:
    xReturn = prvRxCalibrationReply(pxDev,
                                    (xTdmaCalibrationReply_t *) pucFrame,
                                    ullTimeStamp);
    break;
  default:
    return pdFAIL;
  }
  return xReturn;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaSend(xTdmaDev_t *pxDev, uint8_t pucFrame[], uint32_t ulLen)
{
  BaseType_t  xReturn;
  xTdmaDesc_t   pxDesc;

  /* Check if configured */
  if((xTdmaCfgHasSlot() == pdFAIL) || (pxDev->eState != eTdmaStateNormal) ||
      ulLen > pxDev->xCfg.ulMaxSize) return pdFAIL;

  if(ulLen == 22)
    xReturn = 0;

  /* Queue frame */
  pxDesc.pullTimeStamp = NULL;
  pxDesc.pucData       = pucFrame;
  pxDesc.xLen          = ulLen;
  taskENTER_CRITICAL();
  {
    xReturn = prvFifoPut(&pxDev->xTxFifo, &pxDesc);
  }
  taskEXIT_CRITICAL();
  return xReturn;
}
/*---------------------------------------------------------------------------*/

uint8_t *pucTdmaProcessSlotEvent(xTdmaDev_t *pxDev)
{
  xTdmaDesc_t   xDesc;
  uint32_t      ulSlot;

  /* Set next slot event */
  ulSlot = pxDev->xCount.ulSlot++;
  if((pxDev->xCount.ulSlot) < pxDev->xCfg.ulLen) prvSlotEvent(pxDev);
  /* Handle size info*/
  {
    size_t xLen;
    if(prvFifoPeekLen(&pxDev->xTxFifo, &xLen) == pdFAIL) return NULL;
    /* Skip slot if data won't fit. */
    /* TODO: Better solution is using list insted of fifo, and find first
     *       message on the list that fit into slot.*/
    if(xLen > pxDev->xCfg.pxSlots[ulSlot].ulSize) return NULL;
  }
  /* Get data */
  prvFifoGet(&pxDev->xTxFifo, &xDesc);
  /* Send data */
  xHalNetworkInterfaceOutput(xDesc.pucData,
                             xDesc.xLen,
                             xDesc.pullTimeStamp);
  return xDesc.pucData;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaPostProcessSlotEvent(xTdmaDev_t *pxDev, uint8_t *pucData)
{
  if(pucData == NULL) return pdFAIL;
  rtnet_free(pucData);
  if(pxDev->eState == eTdmaStateNormal)
    return xRTnetSendEvent(eRTnetEventTxFrame, NULL, 0, portMAX_DELAY);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

size_t xTdmaFrameSize(eTdmaFrame_t eFrameType)
{
  switch(eFrameType){
  case eTdmaFrameSynchronisation:
    return sizeof(xTdmaSynchronisation_t);
  case eTdmaFrameCalibrationRequest:
    return sizeof(xTdmaCalibrationRequest_t);
  case eTdmaFrameCalibrationReply:
    return sizeof(xTdmaCalibrationReply_t);
  }
  return 0;
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaUpdateLinkStatus(void)
{
  return xHalNetworkInterfaceCheckLinkStatus();
}
/*---------------------------------------------------------------------------*/

BaseType_t  xTdmaIsReady(xTdmaDev_t *pxDev)
{
  if((xTdmaCfgHasSlot() == pdFAIL) || (pxDev->eState != eTdmaStateNormal))
    return pdFALSE;
  return pdTRUE;
}

BaseType_t  prvRxSynchronisation(xTdmaDev_t             *pxDev,
                                 xTdmaSynchronisation_t *pxFrame,
                                 uint64_t                ullRecv)
{
  uint64_t ullXmit;

  pxDev->xCount.ulCycle = rtnet_ntohl(pxFrame->ulCycleNumber);
  pxDev->xCount.ulSlot  = 0;
  pxDev->xT.ullSched    = rtnet_ntohll(pxFrame->ullScheduledTransmissionTime);
  ullXmit               = rtnet_ntohll(pxFrame->ullTransmissionTimeStamp);
  switch(pxDev->eState){
  case eTdmaStatePreInit:
    taskENTER_CRITICAL();
    {
      /* Check the TDMA configuration */
      if(xTdmaCfgHasSlot() == pdPASS){
        xHalTimerSet(ullXmit);
        //pxDev->xT.ullTic = ullXmit;
        pxDev->eState = eTdmaStateInit;
      }
    }
    taskEXIT_CRITICAL();
    break;
  case eTdmaStateInit:
    taskENTER_CRITICAL();
    {
      if(pxDev->xCount.ulInit == 0)
        /* Update global time */
        prvTimerUpdateRelative(pxDev, ullXmit, ullRecv);
      else
        /* Synchronize slave clock with master */
        xHalTimerSync(ullXmit, ullRecv);
      /* Check if initialization phase is coming to an end */
      if(pxDev->xCount.ulInit < tdmaINIT_PHASE_CYCLE_NUMBER)
        pxDev->xCount.ulInit++;
      else pxDev->eState = eTdmaStateCalibrationRequest;
    }
    taskEXIT_CRITICAL();
    break;
  case eTdmaStateCalibrationRequest:
    taskENTER_CRITICAL();
    {
      prvTimerUpdateRelative(pxDev, ullXmit, ullRecv);
      prvSlotEvent(pxDev);
    }
    taskEXIT_CRITICAL();
    if(pxDev->xCount.ulCalib < tdmaCALIB_PHASE_CYCLE_NUMBER){
      if(prvTdmaSendCalibrationRequest(pxDev) == pdPASS){
        pxDev->eState = eTdmaStateCalibrationReplay;
      }
    }
    else{
      pxDev->eState = eTdmaStateNormal;
      xRTnetSendEvent(eRTnetEventTxReady, NULL, 0, portMAX_DELAY);
    }
    break;
  case eTdmaStateCalibrationReplay:
    taskENTER_CRITICAL();
    {
      prvTimerUpdateRelative(pxDev, ullXmit, ullRecv);
      prvSlotEvent(pxDev);
      /* Check if we waiting for replay to long */
      if(pxDev->xCount.ulWait > 0) --pxDev->xCount.ulWait;
      else pxDev->eState = eTdmaStateCalibrationRequest;
    }
    taskEXIT_CRITICAL();
    break;
  case eTdmaStateNormal:
    taskENTER_CRITICAL();
    {
      prvTimerUpdateRelative(pxDev, ullXmit, ullRecv);
      //xHalTimerSync(ullXmit, ullRecv);
      prvSlotEvent(pxDev);
    }
    taskEXIT_CRITICAL();
    xRTnetSendEvent(eRTnetEventSync, NULL, 0, portMAX_DELAY);
    break;
  }
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTdmaSendCalibrationRequest(xTdmaDev_t *pxDev)
{
  xTdmaCalibrationRequest_t *pxFrame = NULL;
  uint32_t                   ulLen;
  uint8_t                   *pucFrame;

  /* Prepare Ethernet frame*/
  pucFrame = pucRTnetPrepareFrame(&ulLen,
                                  sizeof(xTdmaCalibrationRequest_t),
                                  pucRTnetMacMaster(),
                                  eRTnetFrameRTmacTdma);
  if(pucFrame == NULL) return pdFAIL;
  /* Set TDMA frame */
  pxFrame = (xTdmaCalibrationRequest_t*)(pucFrame + ulLen);
  pxFrame->xHeader.usVersion = rtnet_htons(tdmaFRAME_VERSION);
  pxFrame->xHeader.usFrameID = rtnet_htons(tdmaFRAME_ID_CALIBRATION_REQUEST);
  taskENTER_CRITICAL();
  {
    xTdmaSlot_t *pxSlot;
    xTdmaDesc_t  pxDesc;
    uint32_t     ulCycle;
    /* Find slot and cycle for Calibration Request Frame transmission  */
    ulCycle = pxDev->xCount.ulCycle + tdmaCALIB_REQUEST_CYCLE_OFFSET;
    pxSlot  = prvFindFirstSlot(pxDev->xCfg.pxSlots,
                               pxDev->xCfg.ulLen,
                               ulCycle,
                               &ulCycle,
                               0,
                               NULL);
    if(pxSlot == NULL){
      taskEXIT_CRITICAL();
      return pdFAIL;
    }
    /* Find slot and cycle for Calibration Replay Frame reception */
    pxSlot = prvFindFirstSlot(pxDev->xCfg.pxSlots,
                              pxDev->xCfg.ulLen,
                              ulCycle + tdmaCALIB_REQUEST_CYCLE_OFFSET,
                              &ulCycle,
                              0,
                              NULL);
    if(pxSlot == NULL){
      taskEXIT_CRITICAL();
      return pdFAIL;
    }
    pxFrame->ulReplyCycle       = rtnet_htonl(ulCycle);
    pxFrame->ullReplySlotOffset = rtnet_htonll(pxSlot->ullOffset);
    /* Queue frame */
    pxDesc.pullTimeStamp = &pxFrame->ullTransmissionTimeStamp;
    pxDesc.pucData       = pucFrame;
    pxDesc.xLen          = ulLen + sizeof(xTdmaCalibrationRequest_t);
    if(prvFifoPut(&pxDev->xTxFifo, &pxDesc) == pdFAIL){
      taskEXIT_CRITICAL();
      return pdFAIL;
    }
    pxDev->xCount.ulWait = ulCycle - pxDev->xCount.ulCycle + 1;
  }
  taskEXIT_CRITICAL();
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxCalibrationRequest(xTdmaDev_t                *pxDev,
                                      xTdmaCalibrationRequest_t *pxFrame,
                                      uint64_t                   ullRecv)
{
  /* TODO: Implement if master functionality will be supported */
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvRxCalibrationReply(xTdmaDev_t              *pxDev,
                                  xTdmaCalibrationReply_t *pxFrame,
                                  uint64_t                 ullRecvRpl)
{
  uint64_t ullXmitReq, ullRecvReq, ullXmitRpl, ullTrans;
  uint32_t ulCalib;

  if(pxDev->eState != eTdmaStateCalibrationReplay) return pdFAIL;
  /* Update the calibration cycle counter */
  ulCalib = ++pxDev->xCount.ulCalib;
  /* Calculate the the transmission time for the present calibration cycle
   * t_trans = 1/2 * ((T'_recv_rpl - T'_xmit_req) - (T_xmit_rpl - T_recv_req))*/
  ullXmitReq = rtnet_ntohll(pxFrame->ullRequestTransmissionTime);
  ullRecvReq = rtnet_ntohll(pxFrame->ullReceptionTimeStamp);
  ullXmitRpl = rtnet_ntohll(pxFrame->ullTransmissionTimeStamp);
  ullTrans   = ((int64_t)((ullRecvRpl - ullXmitReq) - (ullXmitRpl - ullRecvReq))) >> 1;
  /* Calculate average transmission time
   * t_trans_new = ((i - 1) * t_trans_old + t_trans_pres) / i */
  pxDev->xT.ullTrans =
      ((ulCalib - 1) * pxDev->xT.ullTrans + ullTrans) / ulCalib;
  /* Change TDMA state */
  if(ulCalib < tdmaCALIB_PHASE_CYCLE_NUMBER)
    pxDev->eState = eTdmaStateCalibrationRequest;
  else{
    pxDev->eState = eTdmaStateNormal;
    xRTnetSendEvent(eRTnetEventTxReady, NULL, 0, portMAX_DELAY);
  }
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvTimerUpdateRelative(xTdmaDev_t *pxDev,
                                   uint64_t    ullXmit,
                                   uint64_t    ullRecv)
{
  uint64_t ullPres, ullProc;
  int64_t  llOffs;

  /* t_offs = T_xmit + t_trans - T'_recv*/
  llOffs  = ullXmit + pxDev->xT.ullTrans - ullRecv;
  /* Get actual time */
  xHalTimerGet(&ullPres);
  /* Calculate frame process time t_proc = T' - T'_recv */
  ullProc = ullPres - ullRecv;
  if(ullProc > pxDev->xCfg.ullCycle) ullProc = 0;
  /* T = T' + t_offs + t_proc  */
  xHalTimerSet(ullPres + llOffs + ullProc);
  /* Store t_offs */
  pxDev->xT.llOffs  = llOffs;
  pxDev->xT.ullProc = ullProc;
  xHalTimerSyncInit(ullXmit);
  return pdPASS;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvSlotEvent(xTdmaDev_t *pxDev)
{
  xTdmaSlot_t *pxSlot;
  /* Find slot for this cycle */
  pxSlot = prvFindFirstSlotInCycle(pxDev->xCfg.pxSlots,
                                   pxDev->xCfg.ulLen,
                                   pxDev->xCount.ulCycle,
                                   pxDev->xCount.ulSlot,
                                   &pxDev->xCount.ulSlot);
  if(pxSlot != NULL){
    /* Set slot event: T_slot = T_sched + t_slotoffs*/
    return xHalTimerNewEvent(pxDev->xT.ullSched + pxSlot->ullOffset,
                             pxDev->xCfg.ullCycle);
  }
  return pdFAIL;
}
/*---------------------------------------------------------------------------*/

xTdmaSlot_t *prvFindFirstSlot(xTdmaSlot_t  pxSlots[],
                              uint32_t     ulLen,
                              uint32_t     ulCycleStart,
                              uint32_t    *ulCycleRet,
                              uint32_t     ulIdxStart,
                              uint32_t    *ulIdxRet)
{
  xTdmaSlot_t *pxSlot = NULL;
  uint32_t     ulCycle, ulIdx;

  if(ulLen == 0) return NULL;
  ulCycle = ulCycleStart;
  ulIdx   = ulIdxStart;
  while(pxSlot == NULL){
    pxSlot = prvFindFirstSlotInCycle(pxSlots, ulLen, ulCycle, ulIdx, &ulIdx);
    if(pxSlot != NULL){
      if(ulCycleRet != NULL) *ulCycleRet = ulCycle;
      if(ulIdxRet != NULL) *ulIdxRet = ulIdx;
    }
    else{
      ++ulCycle;
      ulIdx = 0;
    }
  }
  return pxSlot;
}
/*---------------------------------------------------------------------------*/

xTdmaSlot_t *prvFindFirstSlotInCycle(xTdmaSlot_t  pxSlots[],
                                     uint32_t     ulLen,
                                     uint32_t     ulCycle,
                                     uint32_t     ulIdxStart,
                                     uint32_t    *ulIdxRet)
{
  uint32_t ulI;

  for(ulI = ulIdxStart; ulI < ulLen; ++ulI){
    xTdmaSlot_t *pxSlot;
    pxSlot = pxSlots + ulI;
    if((ulCycle % pxSlot->ulPeriod) == (pxSlot->ulPhasing - 1)){
      if(ulIdxRet != NULL) *ulIdxRet = ulI;
      return pxSlot;
    }
  }
  return NULL;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvFifoPeekLen(xTdmaFifo_t *pxFifo, size_t *pxLen)
{
  uint32_t    ulIn, ulOut;
  eTdmaFlag_t eFlag;

  /* Read state into locals */
  ulIn  = pxFifo->ulIn;
  ulOut = pxFifo->ulOut;
  eFlag = pxFifo->eFlag;
  /* Check if data are available */
  if((ulIn != ulOut) || (eFlag == eTdmaFull)){
    xTdmaDesc_t *pxDesc;
    pxDesc = pxFifo->xBuffer + ulOut;
    *pxLen = pxDesc->xLen;
    return pdPASS;
  }
  else return pdFAIL;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvFifoGet(xTdmaFifo_t *pxFifo, xTdmaDesc_t *pxDesc)
{
  uint32_t    ulIn, ulOut;
  eTdmaFlag_t eFlag;

  /* Read state into locals */
  ulIn  = pxFifo->ulIn;
  ulOut = pxFifo->ulOut;
  eFlag = pxFifo->eFlag;
  /* Check if data are available */
  if((ulIn != ulOut) || (eFlag == eTdmaFull)){
    memcpy(pxDesc, pxFifo->xBuffer + ulOut, sizeof(xTdmaDesc_t));
    ulOut++;
    if(ulOut >= rtnetconfigTDMA_FIFO_LENGTH) ulOut = 0;
    pxFifo->ulOut = ulOut;
    pxFifo->eFlag = eTdmaEmpty;
    return pdPASS;
  }
  else return pdFAIL;
}
/*---------------------------------------------------------------------------*/

BaseType_t  prvFifoPut(xTdmaFifo_t *pxFifo, xTdmaDesc_t *pxDesc)
{
  uint32_t    ulIn, ulOut;
  eTdmaFlag_t eFlag;

  /* Read state into locals */
  ulIn  = pxFifo->ulIn;
  ulOut = pxFifo->ulOut;
  eFlag = pxFifo->eFlag;
  /* Check if buffer is not full */
  if((ulIn != ulOut) || (eFlag == eTdmaEmpty))
  { /* Add data to buffer as space is available */
    memcpy(pxFifo->xBuffer + ulIn, pxDesc, sizeof(xTdmaDesc_t));
    ulIn++;
    if(ulIn >= rtnetconfigTDMA_FIFO_LENGTH) ulIn = 0;
    if(ulIn == ulOut) pxFifo->eFlag = eTdmaFull;
    pxFifo->ulIn = ulIn;
    return pdPASS;
  }
  else return pdFAIL;
}
/*---------------------------------------------------------------------------*/
