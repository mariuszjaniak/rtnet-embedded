/*****************************************************************************
 *                                                                           *
 *   rtudp.h                                                                 *
 *                                                                           *
 *   UDP -- real-time User Datagram Protocol                                 *
 *                                                                           *
 *   Copyright (C) 2013 by Mariusz Janiak                                    *
 *   mariusz.janiak@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

#include <string.h>
#include <stdint.h>

/* RTnet includes */
#include "rtnet_inet.h"
#include "rtnet_struct.h"
#include "rtnet_common.h"
#include "rtsocket.h"
#include "rtudp.h"

/*****************************************************************************
 * Constants
 *****************************************************************************/

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

RTNET_STRUCT_BEGIN();
struct xRTUDP_HEADER{
  uint16_t usSourcePort;
  uint16_t usDestinationPort;
  uint16_t usLength;
  uint16_t usChecksum;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTUDP_HEADER xRTudpHeader_t;

RTNET_STRUCT_BEGIN();
struct xRTUDP_PSEUDOHEADER{
  uint32_t ulSourceAddress;
  uint32_t ulDestinationAddress;
  uint8_t  ucZeros;
  uint8_t  ucProtocol;
  uint16_t usUDPLength;
} RTNET_STRUCT_STRUCT;
RTNET_STRUCT_END();
typedef struct xRTUDP_PSEUDOHEADER xRTudpPseudoheader_t;

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

static uint16_t prvUdpChecksum(uint8_t  *pucFrame,
                               uint16_t  usLen,
                               uint32_t  ulSaddr,
                               uint32_t  ulDaddr);

/*****************************************************************************
 *  Global variables
 *****************************************************************************/

/*****************************************************************************
 * Functions implementation
 *****************************************************************************/

BaseType_t  xRTudpProcessIPv4Frame(xRTbuf_t *pxBuf,
                                     uint32_t  ulSaddr,
                                     uint32_t  ulDaddr)
{
  uint8_t        *pucFrame = pxBuf->pucData + pxBuf->ulIdx;
  xRTudpHeader_t *pxHeader = (xRTudpHeader_t *) pucFrame;
  uint16_t        usLen;

  usLen = rtnet_ntohs(pxHeader->usLength);
  if((pxBuf->ulSize - pxBuf->ulIdx) < usLen) return pdFAIL;
  if(pxHeader->usChecksum == 0){;}
  else if(prvUdpChecksum(pucFrame, usLen, ulSaddr, ulDaddr) == 0){;}
  else return pdFAIL;
  pxBuf->ulIdx += sizeof(xRTudpHeader_t);
  {
    xRTnetSockAddr_t xSockAddr;
    xSockAddr.sin_addr = ulSaddr;
    xSockAddr.sin_port = pxHeader->usSourcePort;
    return xRTsocketProcessUDPFrame(pxBuf,
                                    usLen - sizeof(xRTudpHeader_t),
                                    pxHeader->usDestinationPort,
                                    &xSockAddr);
  }
}
/*---------------------------------------------------------------------------*/

uint32_t ulRTudpFrame(uint8_t          *pucFrame,
                      xRTnetSockAddr_t *pxSrcAddr,
                      xRTnetSockAddr_t *pxDstAddr,
                      uint8_t          *pucData,
                      uint16_t          usLen,
                      uint8_t           ucOpt)
{
  xRTudpHeader_t *pxHeader = (xRTudpHeader_t *) pucFrame;
  uint16_t        usSize;

  usSize = xRTudpSize(usLen);
  pxHeader->usSourcePort      = pxSrcAddr->sin_port;
  pxHeader->usDestinationPort = pxDstAddr->sin_port;
  pxHeader->usLength          = rtnet_htons(usSize);
  pxHeader->usChecksum        = 0;
  /* Check if it is not zerocopy mode, if not copy data to frame */
  {
    uint8_t *pucPtr = pucFrame + sizeof(xRTudpHeader_t);
    if(pucPtr != pucData) memcpy(pucPtr, pucData, usLen);
  }
  if(ucOpt & RTNET_SO_UDPCKSUM){
    pxHeader->usChecksum = prvUdpChecksum(pucFrame,
                                          usSize,
                                          pxSrcAddr->sin_addr,
                                          pxDstAddr->sin_addr);
    if(pxHeader->usChecksum == 0) pxHeader->usChecksum = 0xFFFFU;
  }
  return usSize;
}
/*---------------------------------------------------------------------------*/

size_t xRTudpSize(size_t xLen)
{
  return sizeof(xRTudpHeader_t) + xLen;
}
/*---------------------------------------------------------------------------*/

size_t xRTudpHeaderSize(void)
{
  return sizeof(xRTudpHeader_t);
}
/*---------------------------------------------------------------------------*/

uint16_t prvUdpChecksum(uint8_t  *pucFrame,
                        uint16_t  usLen,
                        uint32_t  ulSaddr,
                        uint32_t  ulDaddr)
{
  xRTudpPseudoheader_t *pxPseudo =
      (xRTudpPseudoheader_t *)(pucFrame - sizeof(xRTudpPseudoheader_t));

  pxPseudo->ulSourceAddress      = ulSaddr;
  pxPseudo->ulDestinationAddress = ulDaddr;
  pxPseudo->ucZeros              = 0;
  pxPseudo->ucProtocol           = rtudpIPPROTOCOL;
  pxPseudo->usUDPLength          = rtnet_htons(usLen);
  return rtnet_checksum((uint8_t *) pxPseudo,
                        usLen + sizeof(xRTudpPseudoheader_t));
}
/*---------------------------------------------------------------------------*/
